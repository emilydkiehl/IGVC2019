# Circuitry projects

This directory contains a set of projects for all custom PCB fabrication we
perform on the team.

All projects will use [KiCAD](http://kicad-pcb.org/). KiCAD is free, open source
software that can be used completely without a license.

## Subprojects

+ [JetsonGMSLCSI2](./JetsonGMSLCSI2) - Adapter board for the Jetson TX2 to hook
  up GMSL based cameras
+ [PiCoProcessor](./PiCoProcessor) - Raspberry Pi Hat for controlling the
  kangaroo, power to the kangaroo, estop, remote joystick, and the status lights

## Learning more about KiCad

There is a good introduction to KiCAD on Youtube:

[![KiCAD Quick-Start
Tutorial](https://img.youtube.com/vi/zK3rDhJqMu0/0.jpg)](https://youtu.be/zK3rDhJqMu0)

## Populating the KiCAD default part libraries

We keep a lock-stepped KiCAD parts library. Run the following command to clone
down the public parts libraries onto your machine within this directory:

```
git submodule update --init
```

This will populate the repository with a lot of good default parts!

## Creating a new project

This repository has simple rules for creating a new package within it:

+ Create a new directory under this directory (circuitry). The name should be
  lower case, contain no spaces (use underscores), and its name should be a
  simple description of the package's purpose.
+ Create a simple readme within the directory. This has no defined format, but
  should specify how the circuit works / what it does / where to look to gain
  more information
+ Add a link to the package to this file under the subprojects list using the
  following format:

```
+ [project name](./directory name) - short description of the circuit
```

