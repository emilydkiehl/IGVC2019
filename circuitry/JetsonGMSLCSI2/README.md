# JetsonGMSLCSI2 Board

Continental recently donated cameras which transfer data using the GMSL protocol
over a FAKRA cable. Both of these are automotive specifications, GMSL being a
data transport layer and FAKRA being the physical layer. We plan on using a
Jetson TX2 board for interpreting data received by the camera, but currently
have no way of getting the camera data out of GMSL into a format which the
Jetson TX2 is able to understand. We plan on using the provided CSI-2 lines
which the Jetson supports to import the data in.

# Specification

This board will be designed to simultaneously transcode 3 independent GMSL
streams into 6 CSI-2 channels. We will only populate 2 of these 3 transcoding
circuits for IGVC2019. The board will attach to Jetson TX2 connection J22
(Camera Expansion Conn.) using a Samtec QTH-060-01-H-D-A connector. From this
connector, we will use all 6x2 CSI-2 lanes (4x2 populated). We may also use SPI,
I2C or UART if needed to control the cameras. 2 adjacent CSI-2 lanes will be
allocated to 1 transcoding chip. The pattern for allocation is listed as Figure
15 in the Jetson TX1/TX2 Developer Kit Carrier Board Specification (found in the
docs/ folder). For transcoding, we will use a MAX9290 (datasheet found under
docs/) which takes a single GMSL stream and transcodes it into the 2x2 CSI-2
lanes. Each GMSL stream is connected to a Water Blue coded FAKRA connected for
connection to the camera.

Itemized requirements:

+ Total populated budget under $200

+ Uses MAX9290 for transcoding

+ The Jetson is capable of supplying all power required with cameras over the
  appropriate power rails. Limits are found on Table 36 of the Jetson TX1/TX2
  Developer Kit Carrier Board Specification

+ Must use the Samtec QTH-060-01-H-D-A connector to mate with the JetsonTX2

+ Each FAKRA connector must be coded Water Blue

+ There must be a LED present for displaying status of power

+ There must be LEDs present for feedback regarding errors of any transcoding
  stream

+ Communication required by the MAX9290 must be routed to appropriate
  communication channels on the JetsonTX2

+ (Board Only) The entire board must have all surface mounted components located
  on one side of the board

+ (Board Only) The entire board must not collide with components on the Jetson
  TX2 board

+ (Board Only) Must be able to generate a 3D model of the board to validate the
  previous requirement + for modeling purposes

+ (Board Only) If necessary, standoff holes should be provided so that the board
  can be supported by a 3D structure

