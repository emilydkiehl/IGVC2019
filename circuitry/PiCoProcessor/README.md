# PiCoProcessor Board

The PiCoProcessor board will be responsible for a variety of auxiliary processes
on the robot, including powering and controlling the Kangaroo/Sabertooth motor
controllers, E-Stop circuitry, and LED lighting on the robot. This is all
controlled by a LoRa Feather board.

# Requirements

+ Board must cost under $200
+ Board must fit both the 2x20 pin i/o header and 4 standoff holes on a rasberry pi 3
+ Board must include a Adafruit Feather LoRa header socket
+ Board must allow communication between the Raspberry Pi and the LoRa unit
+ Board must be include either:
  + A RF connector to attach a compatible antenna
  + An onboard antenna
+ Board must be able to communicate with the Kangaroo motor controller via direct raspberry pi communication
+ Board must connect to the Kangaroo using a 4 pin 0.1" header
+ Board must communicate with Neopixel status lights, providing 5V, GND, and Signal
+ Board must include a 1000uF and 100uF capacitor near the Neopixel connector
+ Board must have a way to hook up a 5V 4A supply
+ Board must not collide with the Raspberry Pi
+ Board must expose remaining GPIO pins on the feather for future expansion (start/stop buttons)
