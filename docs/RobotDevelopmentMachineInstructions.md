# Setting up a development machine

This guide assumes you have already installed VirtualBox on your host OS from https://www.virtualbox.org/wiki/Downloads

## Enabling Virtualization
If you are not on a windows machine skip to step 5
1. If you are on a Windows machine, open the task manager.
1. Enable more details and navigate to the performance tab.
1. Navigate to the CPU section.
1. If virtualization is shown as enabled in the bottom right, then you may skip to the next section.
1. If virtualization is not enabled, shut down your machine and open your BIOS.
1. Enable the virtualization setting.

## Installing Ubuntu on VirtualBox

### Creating the VM

1. Open VirtualBox
1. Click the **New** button.
1. Name your VM
1. Choose Linux next to **Type**
1. Choose Ubuntu next to **Version**
1. Click next and choose an amount of at least 2 GB of memory for your VM. 

1. Select the **Create a virtual hard disk now** option.
1. Use defaults of **.vdi** and **dynamically allocated** for setting up hard disk file. 
1. On the page that follows choose the size of the VM's hard disk. A disk size of about 30 GB is absolute minimum for ROS development.
1. Click **Create** to create your VM

### Configuring the VM

1. Select **Settings** in the main virtual box window and navigate to **System**
1. If you want to give more than one CPU core to the VM, navigate to the processor tab and set the amount desired.
1. If you want to enable copy/paste from host to VM (and vice versa) go to the Advanced tab under General and select Bidirectional.

1. Navigate to the **Display** section to increase the VM's video memory.
1. If your system has USB 3.0 ports, make sure to enable them in the **USB** section.
1. Mount the Ubuntu .iso file
    1. Acquire "64-bit PC (AMD64) desktop image" .iso file from http://releases.ubuntu.com/16.04/ 
    1. Navigate to the **Storage** section of the **Settings** menu
    1. Select **Empty** under **Controller: IDE**
    1. Click the disk icon to the right of **Optical Drive** on the right hand of the screen.
    1. Select the file
    
1. Close the **Settings** window and run the Virtual Machine

1. Click **Install Ubuntu**
1. After configuring the keyboard layout, select **Minimal Installation**
1. If installing on a VM, choose **Erase disk and install Ubuntu**
1. Configure user account information.

## Installing Development Tools

### Get git

1. Open a terminal
1. Enter the command: `sudo apt install git`
1. Enter your VM user account password if prompted.
1. Enter `y` if prompted.
1. Set up your credentials
    * `git config --global user.name "John Doe"`
    * `git config --global user.email johndoe@example.com`

## Installing ROS

1. Run the command `sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'`
1. Run the command `sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116`
1. Run the command `sudo apt-get update`
1. For the full install of ROS run: `sudo apt-get install ros-kinetic-desktop-full python-rosinstall python-rosinstall-generator python-wstool build-essential python-catkin-tools`
1. Run `sudo rosdep init`
1. Run `rosdep update`

## Connect to Gitlab

Follow the linked instructions to [create a SSH key](./docs/CreatingSSHKey.md)
