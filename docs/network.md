# Network

This document describes the network of MkII.

As conforming to Dokalman's network, MkII will sit on the 192.168.12.0/24
network. This does not collide with Dokalman's 192.168.11.0/24 network.

MkII also has a secure subnet, 192.168.13.0/24. This sits on top of
192.168.12.0/24, connecting hosts which require transit of 'sensitive' data
through IPSec tunnels.

The following are the IP/MAC address listings for the UNSECURE network

+ 192.168.12.1/24 - 68:72:51:88:B3:8D - bullet.mkii
+ 192.168.12.2/24 - xx:xx:xx:xx:xx:xx - dns.ucnuc0.mkii DNS and DHCP
+ 192.168.12.3/24 - AC-84-C6-7B-14-FA - switch0.mkii
+ 192.168.12.4/24 - AC-84-C6-7B-16-2A - switch1.mkii
+ 192.168.12.5/24 - 00-1C-DC-03-4f-6D - rr.mkii

+ 192.168.12.20/24 - xx:xx:xx:xx:xx:xx - ucnuc0.mkii
+ 192.168.12.21/24 - xx:xx:xx:xx:xx:xx - ucpi0.mkii
+ 192.168.12.22/24 - xx:xx:xx:xx:xx:xx - uctx20.mkii

The radars require special treatment - each separate radar is located on a
separate VLAN located at 192.168.1.2. For each, the NUC will have a special IP
address within the subnet for each VLAN.

VLAN 1x - Radar x

NUC: 192.168.1.1x/24

Where x is 0-5.

The following are the IP/MAC address listings for the SECURE network

+ 192.168.13.2/24 - dns.ucnuc0.sec.mkii DNS and DHCP
+ 192.168.13.20/24 - ucnuc0.sec.mkii
+ 192.168.13.21/24 - ucpi0.sec.mkii
+ 192.168.13.22/24 - uctx20.sec.mkii

+ 192.168.13.100-199/24 - clientxxx.sec.mkii
