# Creating an ssh key
1. Open a terminal and run the following :
```ssh-keygen -t rsa -C "email@example.com"```
1. Press **Enter** to accept the default location and file name.
1. Enter and reenter a passphrase when prompted.
1. Enter ```cat ~/.ssh/id_rsa.pub``` in the terminal.
1. Copy the whole output to the clipboard.
1. Login into Gitlab
1. Navigate to [SSH Keys in Settings](https://gitlab.com/profile/keys).
1. Paste your public key in the key box.
1. Finally, click **Add key**.