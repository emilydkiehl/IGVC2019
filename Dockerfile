FROM ros:melodic-robot
RUN apt-get update && \
    apt-get install -y software-properties-common && \
    apt-add-repository ppa:neovim-ppa/stable && \
    apt-get update && \
    apt-get install -y \
        git \
        libopencv-dev \
        libpcap0.8-dev \
        neovim \
        python3-dev \
        python-catkin-tools \
        libopencv-dev \
        ros-melodic-pcl-ros \
        python-dev \
        python-opencv \
        python-pip \
        python3-dev \
        python-opencv \
        tmux \
        neovim \
        ros-melodic-image-transport \
        git && \
    git clone https://github.com/Ichbinjoe/dotfiles.git /dotfiles && \
    cd /dotfiles && ./install.sh

WORKDIR /ros
