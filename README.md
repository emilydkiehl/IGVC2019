# UC Robotics IGVC 2019

This is the super-repo of all code for the UC Robotics team's entry into IGVC
2019.

The root of this project is a catkin workspace.

UC Robotics uses catkin-tools to compile the project. Once catkin-tools is
installed, the workspace can be build using `catkin build`.

Repository topography:

+ [arduino](./arduino/) - contains all arduino code.
  [arduino/README.md](arduino/README.md) describes projects for individual
  arduino boards
+ [circuitry](./circuitry/) - contains all circuit designs for the robotics
  team. [circuitry/README.md](circuitry/README.md) describes individual circuits
+ [docs](./docs/) - contains all miscellaneous documentation for the robot. For
  learning more about the robot, this is the place to go.
+ [scripts](./scripts/) - contains scripts helpful for developing with the
  robot. See [scripts/README.md](scripts/README.md)
+ [src](./src/) - contains catkin src files. Ros packages are located here. See
  [src/README.md](src/README.md) for descriptions of what is located within each
  package.

# Build Process

In order to validate the code which is within the repository, this is how the
build system works.

We leverage a Gitlab runner. The configuration for this runner is present within
`.gitlab-ci.yml`. The format is defined within the Gitlab documentation.

The actual build process goes through the following steps:

+ We catkin build the workspace

Once all of the steps have completed, the build is finished.

The builder image is located in the `build` branch of this repository.

# Packages you need

For ROS, you will need to install the following packages:

+ libpcap0.8-dev
+ ros-melodic-gps-common
+ ros-melodic-swri-math-util
+ ros-melodic-swri-nodelet
+ ros-melodic-swri-roscpp
+ ros-melodic-swri-serial-util
+ ros-melodic-swri-string-util
+ ros-melodic-tf2-geometry-msgs
+ ros-melodic-tf2-msgs

# Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

# LICENSE

Due to the possibility of the sensors which are located on the robot (and thus
the code located within this repository), this repository is closed source and
should not be shared without prior authorization.

