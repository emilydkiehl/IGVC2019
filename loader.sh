#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. "$DIR/devel/setup.bash"
ROS_MASTER_URI="http://ucnuc0.mkii:11311"
exec "$@"
