#include "Arduino.h"

#include "led.h"
#include "spektrum.h"
#include <SPI.h>
#include <RH_RF95.h>

#define ESTOP_WRITE_PIN 19
#define PI_ESTOP_MASK  0x1
#define REMOTE_ESTOP_MASK  0x2
#define HARD_ESTOP_MASK  0x4

#define OUTSERIAL Serial

#define RFM95_CS 8
#define RFM95_RST 4
#define RFM95_INT 3

#define RF95_FREQ 915.0

RH_RF95 rf95(RFM95_CS, RFM95_INT);

uint8_t reset_lora()
{
    digitalWrite(RFM95_RST, HIGH);
    delay(10);
    digitalWrite(RFM95_RST, LOW);
    delay(10);
    digitalWrite(RFM95_RST, HIGH);
    delay(10);

    if(!rf95.init()){
        return 0;
    }

    if (!rf95.setFrequency(RF95_FREQ))
    {
        return 0;
    }

    rf95.setTxPower(23, false);
    return 1;
}

#define PERFORM_ESTOP_MASK (PI_ESTOP_MASK | REMOTE_ESTOP_MASK)
volatile uint8_t estop_state = HARD_ESTOP_MASK;
volatile uint8_t safety_mode = 0;

/**
 * SETS the estop state. State of 0 indicates ESTOPPED
 */
void set_estop(uint8_t estop_type, uint8_t state)
{
	if (state)
	{
		estop_state |= estop_type;
	} else {
		estop_state &= ~estop_type;
	}
}

void set_pi_estop()
{
    if (k_bfr.hdr.dlen != 1)
        return;

	set_estop(PI_ESTOP_MASK, k_bfr.buf[0]);
}

struct status_response
{
    uint8_t estop_state;
    struct spektrum_ch_data spek;
};

void get_status()
{
    struct status_response* resp = (struct status_response*)k_bfr.buf;
    resp->estop_state = estop_state;

    // We have to update the status of the data before
    // copying over the buffer - otherwise, we may send
    // data which is 'stale' due to a depaired controller
    // which results in no pulses being sent
    spektrum_update_is_valid();
    memcpy(&resp->spek, (void*)&spek_ch_data, 4);
    k_bfr.hdr.dlen = 5;
    kang_send();
}

void set_safety_status()
{
    if (k_bfr.hdr.dlen != 1)
        return;

    safety_mode = k_bfr.buf[0];
}

struct device_command status_commands[3] = {
	{0, get_status},
    {1, set_pi_estop},
    {2, set_safety_status}
};

struct device_command led_commands[1] = {
	{0, led_command}
};

uint8_t devn = 2; //no. of devices
struct device dev[2] = {
	{7, 3, status_commands}, //device id, no. commands, ptr to commands
    {8, 1, led_commands}
};

volatile long last_estop_beacon_sent = 0;
volatile long last_estop_beacon_recvd = 0;
char header[5] = {'u', 'c', 'r', 'o', 'b'};

void estop_dowork(){
    uint8_t buf[6];
    if (millis() - last_estop_beacon_sent > 250)
    {
        // So thats not good...
        rf95.setMode(RHGenericDriver::RHModeIdle);
        memcpy(buf, header, 5);
        buf[5] = estop_state & PI_ESTOP_MASK;
        rf95.send(buf, 6);

        last_estop_beacon_sent = millis();
    }

    uint8_t read = 6;
    if (rf95.recv(buf, &read))
    {
        if (read != 6)
        {
            return;
        }
        
        if (memcmp(buf, header, 5))
        {
            return;
        }

        set_estop(REMOTE_ESTOP_MASK, !buf[5]);

        last_estop_beacon_recvd = millis();
    } else if (millis() - last_estop_beacon_recvd > 1000) 
    {
        set_estop(REMOTE_ESTOP_MASK, 0);
    }
}

void setup() {
    OUTSERIAL.begin(9600);
    Serial1.begin(38400);
//    while(!OUTSERIAL);
    
    pinMode(RFM95_RST, OUTPUT);

    pinMode(ESTOP_WRITE_PIN, OUTPUT);
    digitalWrite(ESTOP_WRITE_PIN, HIGH);

    kang_init();
    spektrum_setup();
    led_init();
    if (!reset_lora())
        while(1);

    pinMode(5, OUTPUT);
    digitalWrite(5, HIGH);
}

void loop()
{
    led_tick();

    kang_dowork();
    estop_dowork();

    digitalWrite(ESTOP_WRITE_PIN, !safety_mode || (millis() - last_message) < 1000);
}

