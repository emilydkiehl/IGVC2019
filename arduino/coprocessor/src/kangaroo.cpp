#include "kangaroo.h"
#include "HardwareSerial.h"
#include "Arduino.h"

#define KSERIAL Serial1
struct kang_bfr k_bfr; 

unsigned long last_message = 0;
unsigned long last_byte = 0;

struct { 
    uint8_t write : 1;
    uint8_t hld   : 1;
    uint16_t ptr  : 14;
} k_stat;

void kang_init()
{
    k_stat.write = 0;
    k_stat.hld = 0;
    k_stat.ptr = 0;

    KSERIAL.setTimeout(0);
}

void kang_dowrite()
{
    // we need to write our payload
    uint16_t to_write = KSERIAL.availableForWrite();
    if (to_write <= 0)
        return; // Nothing to do
   
    // header + dlen + crc
    uint16_t bfr_len = k_bfr.hdr.dlen + sizeof(struct kang_hdr) + 2;
    uint16_t bfr_left = bfr_len - k_stat.ptr;

    // don't write more than we need
    if (to_write > bfr_left)
        to_write = bfr_left;

    size_t sent = KSERIAL.write(&((uint8_t*)&k_bfr)[k_stat.ptr], to_write);
    k_stat.ptr += sent;

    if (k_stat.ptr - bfr_len <= 0) {
        // we wrote everything! reset for a read.
        k_stat.write = 0;
        k_stat.ptr = 0;
    }
}

uint16_t crc14(const uint8_t* data, size_t length)
{
    uint16_t crc = 0x3fff; size_t i, bit;
    for (i = 0; i < length; i ++)
    {
        crc ^= data[i] & 0x7f;
        for (bit = 0; bit < 7; bit ++)
        {
            if (crc & 1) 
            { 
                crc >>= 1; 
                crc ^= 0x22f0; 
            }
            else
            {
                crc >>= 1;
            }
        }
    }
    return crc ^ 0x3fff;
}

struct device_command *kang_findCmd()
{
    for (int i = 0; i < devn; i++)
    {
        struct device *d = &dev[i];
        if (d->dev_id != k_bfr.hdr.addr)
            continue;

        for (int j = 0; j < d->cmdn; j++)
        {
            struct device_command *c = &d->cmd[j];

            if (c->cmd_id != k_bfr.hdr.cmd)
                continue;
            return c;
        }
    }
    return NULL;
}

void kang_execute66()
{
    // we need to first validate the CRC
    // because the kangaroo protocol is FUCKING STUPID, we have to rip off the
    // address bit from the header.
    if (!(k_bfr.hdr.addr & 0x80))
        return;

    // kill me.
    k_bfr.hdr.addr &= 0x7f;

    // first, determine if this is something we care about
    struct device_command *c = kang_findCmd();
    if (c == NULL) {
        return; // discard
    }

    // make sure the CRC matches
    uint16_t calculated_crc = crc14((uint8_t*)&k_bfr, sizeof(struct kang_hdr) + k_bfr.hdr.dlen);
    uint8_t* crc = &k_bfr.buf[k_bfr.hdr.dlen];
    uint16_t packet_crc;
    packet_crc = crc[1] & 0x7f;
    packet_crc <<= 7;
    packet_crc |= crc[0] & 0x7f;
    if (packet_crc ^ calculated_crc) {
        return;
    }

    last_message = millis();

    c->on_query();
}

void kang_doread()
{
    size_t can_read = KSERIAL.available();
    while (can_read)
    {
        last_byte = millis();
        // we can tell where we are in the protocol by reading the ptr value.
        // if we are less than 3 bytes in, we don't know how long the entire
        // packet will be
        // we know at the very least that we will need to read the rest of the
        // header and the CRC (5)
        uint16_t total_read = 2 + sizeof(struct kang_hdr);
       
        // otherwise, we need to add on the actual size of data as well.
        if (k_stat.ptr >= sizeof(struct kang_hdr)) 
            total_read += k_bfr.hdr.dlen;


        // only try to read what is available
        if (total_read > can_read)
            total_read = can_read;

        size_t read = KSERIAL.readBytes(&((char*)&k_bfr)[k_stat.ptr], total_read);

        k_stat.ptr += read;
        can_read -= read;

        if (k_stat.ptr < 3)
            continue; // we didn't read enough to know how much data we need to complete

        // WE HAVE READ ENOUGH
        if (k_stat.ptr >= 2 + sizeof(struct kang_hdr) + k_bfr.hdr.dlen)
        {
            // perform a command execution
            kang_execute66();

            // if after execute66 we flipped into write mode and dind't finish,
            // OR if we are instead holding, we need to respect that and not
            // try to read again.
            if (k_stat.hld == 1 || k_stat.write == 1)
                return;
    
            // otherwise, assume k_stat wasn't reset.
            k_stat.ptr = 0;
            // aaaaand read again
        }
    }

    if ((millis() - last_byte) > 250)
    {
        k_stat.ptr = 0;
        last_byte = millis();
    }
}

void kang_send()
{
    k_stat.write = 1;
    k_stat.hld = 0;
    k_stat.ptr = 0;

    uint16_t calculated_crc = crc14((uint8_t*)&k_bfr, sizeof(struct kang_hdr) + k_bfr.hdr.dlen);
    uint8_t* crc = &k_bfr.buf[k_bfr.hdr.dlen];
    crc[0] = calculated_crc & 0x7f;
    calculated_crc >>= 7;
    crc[1] = calculated_crc & 0x7f;

    kang_dowrite();
}


void kang_dowork()
{
    
    if (k_stat.hld)
        return; // no work to do

    if (k_stat.write) 
    {
        kang_dowrite();
    }

    if (!k_stat.write)
    {
        kang_doread();
    }
}

void kang_hold()
{
    k_stat.hld = 1;
}
