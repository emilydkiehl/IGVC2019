#include <Arduino.h>
#include "spektrum.h"

#define SENSE0 19
#define SENSE1 18
#define SENSE2 17
#define SENSE3 9

volatile unsigned long lch_start[3];
volatile unsigned long ch_start[3];
volatile uint8_t history[21];
volatile uint8_t hidx;

volatile struct spektrum_ch_data spek_ch_data;

void ch0_change()
{
    if (digitalRead(SENSE2) == HIGH)
    {
        lch_start[0] = ch_start[0];
        ch_start[0] = micros();
    } else 
    {
        unsigned long pulse = micros() - ch_start[0];
        if (pulse > 4000)
            return;

        pulse = constrain(pulse, 900, 2100);
        spek_ch_data.ch0 = (pulse - 900) / 5;
    }
}

void ch1_change()
{
    if (digitalRead(SENSE3) == HIGH)
    {
        lch_start[1] = ch_start[1];
        ch_start[1] = micros();
    } else
    {
        unsigned long pulse = micros() - ch_start[1];
        
        if (pulse > 4000)
            return;

        pulse = constrain(pulse, 900, 2100);
        spek_ch_data.ch1 = (pulse - 900) / 5;
    }
}

void ch2_change()
{
    if (digitalRead(SENSE1) == HIGH)
    {
        lch_start[2] = ch_start[2];
        ch_start[2] = micros();
    } else
    {
        unsigned long pulse = micros() - ch_start[2];
        
        if (pulse > 4000)
            return;

        int valid = pulse > 1500 ? 1 : 0;
        history[hidx++] = valid;
        if (hidx >= 21) 
            hidx = 0;
        uint8_t yvotes = 0;

        for (uint8_t i = 0; i < 21; i++)
            if (history[i])
                yvotes++;


        spek_ch_data.is_enabled = yvotes > 10;
    }
}

void spektrum_update_is_valid()
{
    unsigned long now = micros();
    unsigned long longest_diff = 0;
    for (size_t i = 0; i < 3; i++) 
    {
        unsigned long diff = now - lch_start[i];
        if (diff > longest_diff)
            longest_diff = diff;
    }

    spek_ch_data.is_valid = longest_diff < 250000;
}

void spektrum_setup()
{
    memset((void*)&spek_ch_data, 0, sizeof(struct spektrum_ch_data));
    memset((void*)lch_start, 0, sizeof(long) * 3);
    memset((void*)ch_start, 0, sizeof(long) * 3);
    memset((void*)history, 0, sizeof(uint8_t) * 3);
    hidx = 0;

    pinMode(SENSE1, INPUT);
    pinMode(SENSE2, INPUT);
    pinMode(SENSE3, INPUT);

    // NOTE: I'm well aware this makes 0 sense, just roll with it
    attachInterrupt(SENSE2, ch0_change, CHANGE);
    attachInterrupt(SENSE3, ch1_change, CHANGE);
    attachInterrupt(SENSE1, ch2_change, CHANGE);
}
