#include "led.h"

#define LED_PIN 10

Adafruit_NeoPixel_ZeroDMA *strip;
union led_state global_led_state;

uint32_t black;

uint32_t led_colors[5] = {
    0, // RED, below
    0, // GREEN
    0, // BLUE
    0, // ORANGE
    0  // YELLOW
};

uint8_t current_color = 0;
uint8_t current_style = 0;

unsigned long previous_tick_time = 0;

void led_command() {
    struct LEDPacket * packet = (struct LEDPacket*)k_bfr.buf;
    // Bounds checking
    if (packet->color > 4 || packet->style > 5) return;
    
    // Lets first figure out if we really need to change
    if (packet->color == current_color && packet->style == current_style)
        return;

    current_color = packet->color;
    current_style = packet->style;

    led_styles[current_style].init();
}


void led_tick() 
{
    led_styles[current_style].tick();
}

void fill_chain_with_color()
{
    for (size_t i = 0; i < LED_COUNT; i++)
    {
		strip->setPixelColor(i, led_colors[current_color]);
    }
    strip->show();
}

void solid_init()
{
    global_led_state.solid.last_update = millis();
    fill_chain_with_color();
}

void solid_tick()
{
    long now = millis();
    // We send an updated state to the LEDs every 5 seconds because otherwise if
    // a power issue occurs, the LEDs will not relight until a state change
    if (now - global_led_state.solid.last_update > 5000)
    {
        strip->show();
        global_led_state.solid.last_update = now;
    }
}

void blink_init(unsigned long period)
{
    global_led_state.blink.last_rising_edge = millis();
    global_led_state.blink.period = period;
    global_led_state.blink.up = 1;
    fill_chain_with_color();

}

void blink_fast_init()
{
    blink_init(250);
}

void blink_slow_init()
{
    blink_init(1000);
}

void blink_tick()
{
    unsigned long now = millis();
    if (global_led_state.blink.up)
    {
        // We are present in the first half of our animation
        // Lets make sure we haven't passed our period time
        if (now - global_led_state.blink.last_rising_edge >
                global_led_state.blink.period)
        {
            // Fill the strip with black
            for(int i = 0; i < LED_COUNT; i++)
                strip->setPixelColor(i, black);

            strip->show();
            global_led_state.blink.up = 0;
        }
    } else
    {
        // We are in the second half of our animation
        // If we pass period * 2, we have completed the cycle, so reset.
        unsigned long end_of_cycle = global_led_state.blink.last_rising_edge +
            (global_led_state.blink.period * 2);
        if (now > end_of_cycle)
        {
            fill_chain_with_color();
            // We mark this with end_of_cycle instead of now because we want to
            // narrow down the duty cycle to exactly 50% as much as possible.
            // If we used now, its possible that 'end of cycle' streatches longer
            // than it should, skewing the time-base of the rising edge over time
            global_led_state.blink.last_rising_edge = end_of_cycle;
            global_led_state.blink.up = 1;
        }
    }
}

void seq_init(int width, long speed)
{
    global_led_state.seq.width = width;
    global_led_state.seq.step = 0;
    global_led_state.seq.step_speed = speed;
    global_led_state.seq.last_step_time = millis();

    for(int i = 0; i < width; i++)
		strip->setPixelColor(i, led_colors[current_color]);

    for(int i = width; i < LED_COUNT; i++)
		strip->setPixelColor(i, black);
    
    strip->show();
}

void seq_tick()
{
    if(global_led_state.seq.last_step_time + global_led_state.seq.step_speed > millis())
        return;

    strip->setPixelColor(global_led_state.seq.step, black);
    strip->setPixelColor((global_led_state.seq.step + global_led_state.seq.width) % LED_COUNT, led_colors[current_color]);

    strip->show();
   
    global_led_state.seq.step++;
    if (global_led_state.seq.step > LED_COUNT)
        global_led_state.seq.step = 0;

    global_led_state.seq.last_step_time = millis();
}

void idle_seq_init()
{
	seq_init(50, 100);
}

void slow_seq_init()
{
	seq_init(30, 500);
}

void fast_seq_init()
{
	seq_init(10, 100);
}

LEDPatternDef led_styles[6] = {
    {solid_init, solid_tick}, // solid
    {blink_slow_init, blink_tick}, // slow_blink
    {blink_fast_init, blink_tick}, // fast_blink
    {idle_seq_init, seq_tick}, // idle_sequence
    {slow_seq_init, seq_tick}, // slow_sequence
    {fast_seq_init, seq_tick}  // fast_sequence
};

void led_init()
{
    strip = new Adafruit_NeoPixel_ZeroDMA(LED_COUNT, 11, NEO_GRB + NEO_KHZ800);
    led_colors[0] = strip->Color(255, 0, 0);
    led_colors[1] = strip->Color(0, 255, 0);
    led_colors[2] = strip->Color(0, 0, 255);
    led_colors[3] = strip->Color(255, 165, 0);
    led_colors[4] = strip->Color(255, 255, 0);
    black = strip->Color(0, 0, 0);
    strip->begin();
    led_styles[current_style].init();
}

