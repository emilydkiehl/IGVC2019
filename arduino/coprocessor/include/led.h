#include <Adafruit_NeoPixel_ZeroDMA.h>
#include "kangaroo.h"

#define LED_COUNT 126
extern Adafruit_NeoPixel_ZeroDMA *strip;

extern union led_state global_led_state;

typedef void(*led_action)();

struct led_solid_state
{
    unsigned long last_update;
};

struct led_blink_state
{
    unsigned long last_rising_edge;
    unsigned long period;
    uint8_t up;
};

struct led_seq_state
{
    unsigned int width;
    unsigned int step;
    unsigned long step_speed;
    unsigned long last_step_time;
};

union led_state {
    struct led_solid_state solid;
    struct led_blink_state blink;
    struct led_seq_state seq;
};

struct LEDPacket {
    uint8_t color;
    uint8_t style;
};

struct LEDPatternDef {
    // Should initialize any state needed within the global_led_state.
    // should also set the initial state of the leds buffer.
    led_action init;
    // Should either progress the animation or do nothing. Called every loop.
    led_action tick;
};

#define LED_COLOR_RED 0
#define LED_COLOR_GREEN 1
#define LED_COLOR_BLUE 2
#define LED_COLOR_ORANGE 3
#define LED_COLOR_YELLOW 4

#define LED_STYLE_SOLID 0
#define LED_STYLE_SLOW_BLINK 1
#define LED_STYLE_FAST_BLINK 2
#define LED_STYLE_IDLE_SEQUENCE 3
#define LED_STYLE_SLOW_SEQUENCE 4
#define LED_STYLE_FAST_SEQUENCE 5

extern uint32_t led_colors[5];
extern LEDPatternDef led_styles[6];
extern uint8_t current_color;
extern uint8_t current_style;

void led_command();
void led_tick();
void led_init();
