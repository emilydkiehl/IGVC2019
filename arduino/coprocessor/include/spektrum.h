#pragma once

struct spektrum_ch_data{
    volatile unsigned char is_valid : 8;
    volatile unsigned char is_enabled : 8;
    volatile unsigned char ch0 : 8;
    volatile unsigned char ch1 : 8;
};

extern volatile struct spektrum_ch_data spek_ch_data;

void spektrum_update_is_valid();
void spektrum_setup();
