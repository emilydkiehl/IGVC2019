
#include <Arduino.h>

struct kang_hdr {
    uint8_t addr;
    uint8_t cmd;
    uint8_t dlen;
};

typedef void (*handle_command)(void);

struct device_command {
    uint8_t cmd_id;
    handle_command on_query;
};

struct device {
    uint8_t dev_id;
    uint8_t cmdn;
    struct device_command *cmd;
};

struct kang_bfr {
    struct kang_hdr hdr;
    uint8_t buf[258];
};

extern struct kang_bfr k_bfr;

extern unsigned long last_message;

extern uint8_t devn;
extern struct device dev[2];

// This method initializes internal state tracking for the kangaroo protocol
void kang_init();

// This method throws the buffer into write mode. It will attempt to process it
// immediately, then continue processing in kang_dowork()
void kang_send();

// This is the looped method that should be called every 'tick'. It will
// _never_ cpu block.
void kang_dowork();

// This 'holds' kangaroo operations until kang_send() is called. Used for asynch
// operations which aren't able to return writes immediately
void kang_hold();
