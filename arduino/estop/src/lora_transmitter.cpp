#include <Arduino.h>
#include <SPI.h>
#include <RH_RF95.h>
#include <LowPower.h>

#define RFM95_CS 8
#define RFM95_RST 4
#define RFM95_INT 3

#define RF95_FREQ 915.0

RH_RF95 rf95(RFM95_CS, RFM95_INT);

#define ESTOP_PIN 12
#define RESET_PIN 13

long last_interact;
uint8_t estop_state = 0;
uint8_t lora_ok = 0;
uint8_t needs_preempt = 0;

char header[5] = {'u', 'c', 'r', 'o', 'b'};

void estop() 
{
    if (estop_state == 0)
        needs_preempt = 1;

    estop_state = 1;
    last_interact = millis();
}

void reset() 
{
    if (estop_state == 1)
        needs_preempt = 1;

    estop_state = 0;
    last_interact = millis();
}

uint8_t reset_lora()
{
    digitalWrite(RFM95_RST, HIGH);
    delay(10);
    digitalWrite(RFM95_RST, LOW);
    delay(10);
    digitalWrite(RFM95_RST, HIGH);
    delay(10);

    if(!rf95.init()){
        lora_ok = 0;
        return 0;
    }

    if (!rf95.setFrequency(RF95_FREQ))
    {
        lora_ok = 0;
        return 0;
    }

    rf95.setTxPower(23, false);
    lora_ok = 1;
    return 1;
}

void perform_sleep()
{

    // Turn off the radio
    rf95.sleep();

    // Detach everyone from USB
    USBDevice.detach();

    // Go to sleep!
    LowPower.standby();
    
    // Attach the usb device again
    USBDevice.attach();

    // Wake up the radio
    reset_lora();
}

void loop()
{
    long now = millis();
    if (now - last_interact > 300000)
    {
        perform_sleep();
        now = millis(); // We overwrite this because time probably changed
        last_interact = now;
    }
    if (!lora_ok)
    {
        if (!reset_lora())
            return;
    }

    uint8_t buf[6];
    uint8_t read = 6;
    if (rf95.recv(buf, &read))
    {
        if (read != 6)
        {
            return;
        }

        if (memcmp(buf, header, 5))
        {
            return;
        }

        Serial.println(buf[5]);
        if (buf[5])
        {
            // The remote robot is running, we should stay alive
            last_interact = now;
        }
        // Repond with our beacon
        buf[5] = estop_state;
        rf95.send(buf, 6);
        needs_preempt = 0;
    } else if (needs_preempt)
    {
        // Repond with our beacon
        memcpy(buf, header, 5);
        buf[5] = estop_state;
        rf95.send(buf, 6);

        needs_preempt = 0;
    }

    delay(50);
}


void setup()
{
    pinMode(ESTOP_PIN, INPUT_PULLUP);
    pinMode(RESET_PIN, INPUT_PULLUP);

    attachInterrupt(ESTOP_PIN, estop, LOW);
    attachInterrupt(RESET_PIN, reset, LOW);

    pinMode(RFM95_RST, OUTPUT);

    if (!reset_lora())
        while(1);

    last_interact = millis();
    estop_state = 1;
}
