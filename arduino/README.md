# Arduino projects

## Subprojects
+ None yet :(

## Creating a new project

This repository has simple rules for creating a new project within it:

+ Create a new directory under this directory (arduino). The name should be
  lower case, contain no spaces (use underscores), and its name should be a
  simple description of the project's function.
+ Create a simple readme within the directory. This has no defined format, but
  should specify how the program works / what it does.
+ Add a link to the project to this file under the subprojects list using the
  following format:
```
+ [project name](./directory name) - short description of the project
```
