Requirements for Coprocessor Firmware

Enable and disable the ESTOP via pin 26 PD6, including through service. Be able to report on ESTOP state through service.

Set a string of LEDs to a color pattern as defined by a service.
+ LED pin 30 (PB6)
+ Colors: RED(0) GREEN(1) BLUE(2) ORANGE(3) YELLOW(4)
+ Styles: SOLID(0) SLOW_BLINK(1) FAST_BLINK(2) IDLE_SEQUENCE(3) SLOW_SEQUENCE(4) FAST_SEQUENCE(5)
+ Channel led Command 0 Data bytearray([color, style]) No reply

Read 12 and 5V reference lines

+ 5V Ref (1.667V input) Pin 36 PF7
+ 12V Ref (1.667V input) Pin 37 PF6

Set and read fan speeds

+---------+---------+-----------+
| Channel | PWM Pin | Sense Pin |
+---------+---------+-----------+
| Chan0   | 31 PC6  | 41 PF0    |
| Chan1   | 27 PD7  | 40 PF1    |
| Chan2   | 29 PB5  | 39 PF4    |
| Chan3   | 12 PB7  | 39 PF5    |
+---------+---------+-----------+

+ https://en.wikipedia.org/wiki/Computer_fan_control

Communicate with the temperature sensor over I2C. Expose as a service returning the temperature. I2C Addr 0x48

+ https://www.mouser.com/datasheet/2/308/NCT75-D-62555.pdf
