# How to contribute

This repository is Git versioned. Since many people who are active on the UC
Robotics team do not have experience with git, below is a generic workflow for
contributing changes:

+ Find the issue you want to work on from the [issues
  list](https://gitlab.com/UCRobotics/IGVC2019/issues)
+ Create a merge request and new branch ![Create a Merge Request and New
  Branch](./docs/pics/contributing_mr_and_branch.png)
+ Run the following command to update your current working view to your new
  working branch: `git pull origin <branch name>` in the project directory where
  you are working.
+ Work on your changes. Add or remove any files you may want, and make sure
  everything compiles!
+ `git add ...` - Add all of the files you want to commit. For example, if you
  edited the `code.cpp` file, you would run `git add code.cpp`. You can chain
  multiple file names onto this command ex. `git add code.cpp code.h Makefile`
+ `git commit` - Commits all of your added files. Write a descriptive message of
  what you changed.
+ `git push origin <branch name>` - pushes up your branch to the repository.
+ Once the build on your merge request passes, ask a privileged member of the UC
  Robotics team to review your changes and merge them.

## Tips and tricks with Git

### What files have I changed? What is staged?

`git status` shows the current status of your repository - files which have been
deleted, added, or modified. Only files staged will be committed when `git
commit` is run. You can use `git add` to add files to be committed.

### I added a file I didn't want to add. How can I remove it?

A variation of `git rm` usually works. If you also want to remove the file on
the file system, run `git rm --cached <filename>`. Repositories can be removed
with the `-r` flag: `git rm --cached -r <directory name>`.

### What branch am I on?

`git branch` tells you information about branches. You can switch which branch
you are on using `git branch <branch name>`.

### I want to do a git operation, but it complains I have uncommitted changes.

You can usually stash and unstash your changes if you need to do a quick git
operation. `git stash` takes all of your unstaged changes and stores them away.
Perform your git operation, then do `git stash apply` to reapply your changes.
Your changes may not apply cleanly, and you will need to merge a diff.

## Suggested GUIs for Git

Git is hard to pick up for a beginner. Knowing the above, new users of git may
be more comfortable using the following software:

+ [GitKraken](https://www.gitkraken.com/download) - Linux, Mac OS and Windows
  client
+ [SourceTree](https://www.sourcetreeapp.com/) - Mac OS & Windows client made by
  Atlassian.

Make sure when using this software that you are performing similar operations as
described above. If you need help, feel free to ask a more experienced member of
UC Robotics.
