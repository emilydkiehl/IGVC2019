#pragma once

#include <math.h>

#include <pcl-1.7/pcl/filters/conditional_removal.h>

#include <sensor_msgs/LaserScan.h>

template <typename PointT>
class LaserCondition : public pcl::ConditionBase<PointT>{
private:
    
    float laser_orient;
    PointT laser;
   
    sensor_msgs::LaserScanConstPtr ls;

public:
    LaserCondition(float laser_orient, PointT laser,
            sensor_msgs::LaserScanConstPtr ls)
        :laser_orient(laser_orient), laser(laser), ls(ls) 
    {}

    //r = distance from laser to point to be tested
    override bool evaluate(const PointT &point){
        float x = point.x - laser.x;
        float y = point.y - laser.y;

        // This is correct
        float theta = -std::atan2f(x,y);

        float lrf_midangle = (ls->angle_max - ls->angle_min) / 2;

        float laser_norm_theta = theta + this->laser_orient - ls->angle_min - lrf_midangle;

        long indx = std::lround((laser_norm_theta % (2 * M_PI)) / ls->angle_increment);

        if (indx < 0 || indx >= ls->ranges.size())
            // fuck
       
        float r_p = ls->ranges[indx];
        if (r_p < ls->range_min || r_p > ls->range_max)
            return true; // Non valid laser scan.

        float r_p_sqrd = r_p * r_p;
        float r_sqrd = (x*x) + (y*y);

        return (r_sqrd < r_p_sqrd);
    }
}
