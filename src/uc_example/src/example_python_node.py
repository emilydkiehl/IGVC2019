#!/usr/bin/env python2
# We need the line above because otherwise roslaunch doesn't know how to
# interpret this file

# We have to import rospy so we can hook it up to ros
import rospy

def main():
    # Starts the new 'example_node'
    rospy.init_node("example_node", anonymous=True)
    rospy.spin()

# This guards us from starting a node if we accidentally import this file
# instead
if __name__ == "__main__":
    main()
