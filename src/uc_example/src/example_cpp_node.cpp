#include "ros/ros.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "example_cpp");

    ros::NodeHandle nh;
    ros::spin();
}
