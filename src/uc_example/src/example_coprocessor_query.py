#!/usr/bin/env python

# You can run this fine node here using the following command:

# rosrun uc_example example_coprocessor_query.py

import rospy

from uc_msgs.srv import CoprocessorMsg, CoprocessorMsgRequest

def do():
    rospy.init_node("coprocessor_query_node", anonymous=True)
    
    rospy.wait_for_service("coprocessor", timeout=5)
    svc = rospy.ServiceProxy("coprocessor", CoprocessorMsg)
    rospy.loginfo("passed into service")
    req = CoprocessorMsgRequest(
            channel="test",
            command=74,
            data=[1, 2, 3, 4, 5, 6, 7, 8],
            reply=True)
    rospy.loginfo("finished loading request")
    resp = svc(req)
    rospy.loginfo("done")
    print(len(resp.data))

if __name__ == '__main__':
    do()
