# Ros packages

## Packages
+ [novatel_gps_driver](./novatel_gps_driver) - Contains modified code for the
  novatel gps driver to also include GPHDT message
+ [novatel_gps_msgs](./novatel_gps_msgs) - Contains messages from
  novatel_gps_driver
+ [uc_example](./uc_example) - Contains an example C++ and Python node with
  supporting launch files
+ [uc_master](./uc_master) - Contains most of the ROS configuration, as well as
  some other various nodes
+ [uc_msgs](./uc_msgs) - Contains all custom messages and services for the
  robot
+ [uc_vision](./uc_vision) - Contains vision code
+ [uc_pointcloud](./uc_pointcloud) - Contains lidar code and parse function 
  for lidar and vision

## Creating a new package

This repository has simple rules for creating a new package within it:

+ Create a new directory under this directory (src). The name should be
  lower case, contain no spaces (use underscores), and its name should be a
  simple description of the package's purpose.
+ Create a simple readme within the directory. This has no defined format, but
  should specify how the package works / what it does / where to look to gain
  more information
+ Add a link to the package to this file under the packages list using the
  following format:
```
+ [package name](./directory name) - short description of the package
```
