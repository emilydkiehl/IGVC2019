#pragma once

#include <hardware_interface/robot_hw.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>

#include <marsupial_driver/kangaroo.h>

bool shouldIssueCommands();

class UCRobotHW : public hardware_interface::RobotHW
{
public:
    UCRobotHW(ros::NodeHandle &nh, kangaroo::Kangaroo *k, double ticksPerMeter);

    void read(const ros::Time &time, const ros::Duration &period) override;
    void write(const ros::Time &time, const ros::Duration &period) override;
    void halt();
private:
    hardware_interface::JointStateInterface joint_state_interface;
    hardware_interface::VelocityJointInterface velocity_joint_interface;

    kangaroo::Kangaroo *k;

    double ticksPerRadian;

    struct {
        double pos;
        double pos_offset;
        double vel;
        double eff;
        double velcmd;
        bool shouldInvert;
    } states[2];
};
