#include <marsupial_driver/kangaroo.h>

using namespace std;
using namespace ros;
using namespace uc_msgs;

namespace kangaroo {
    
    const std::string KANG_ERR_MSG[static_cast<uint8_t>(kangaroo::Error::ErrorCount) + 1] = {
        "No error occurred.",
        "The channel is not started. Command Start on the channel.",
        "The channel needs to be homed. Command Home on the channel.",
        "A control error has occurred. Command Start on the channel to clear the control error.",
        "The controller is in the wrong mode. For example, the DIP switches may be in mixed mode while the tune was done in independent mode.",
        "The parameter is unknown.",
        "A serial timeout occurred, or the TX line was disconnected. Command Start on the channel to clear the serial timeout.",
        "Response was detected to be malformed in some way.",
        "An invalid error code was returned."
    };

    bool Kangaroo::SendData(uc_msgs::CoprocessorMsg &msg) {
        msg.request.channel = this->channelName;
        return this->rpcClient.call(msg);
    }
    
    void ChannelRef::Start() {
        CoprocessorMsg m;
        m.request.command = 32;
        
        m.request.data.push_back(this->channel);
        m.request.data.push_back(0);

        m.request.reply = false;

        parent->SendData(m);
    }

    void ChannelRef::SetUnits(KangarooBitpacked desired_units, KangarooBitpacked machine_units) {
        CoprocessorMsg m;
        m.request.command = 33;
        m.request.data.push_back(this->channel);
        m.request.data.push_back(0);

        ChannelRef::PackNumber(m.request.data, desired_units);
        ChannelRef::PackNumber(m.request.data, machine_units);
        
        m.request.reply = false;
       
        parent->SendData(m);
    }

    void ChannelRef::Home() {
        CoprocessorMsg m;
        m.request.command = 34;
        m.request.data.push_back(this->channel);
        m.request.data.push_back(0);
        m.request.reply = false;

        parent->SendData(m);
    }
   
    void ChannelRef::System(uint8_t sys_cmd, 
            const std::vector<KangarooBitpacked> &params) {
        CoprocessorMsg m;
        m.request.command = 37;
        m.request.data.push_back(this->channel);
        m.request.data.push_back(0);
        m.request.data.push_back(sys_cmd);
        for (auto i = params.cbegin(); i != params.cend(); i++)
        {
            ChannelRef::PackNumber(m.request.data, *i);
        }
        m.request.reply = false;

        parent->SendData(m);
    }

    void ChannelRef::PowerDown() {
        std::vector<KangarooBitpacked> p;
        this->System(0, p);
    }

    void ChannelRef::PowerDownAll() {
        std::vector<KangarooBitpacked> p;
        this->System(1, p);
    }

    bool ChannelRef::EnsureInit() {
        if (initialized) return true;
    
        ROS_INFO("Starting channel %c", this->channel);

        Start();
        Home();

        auto start = ros::Time::now();
        do {
            KangarooBitpacked ignored;
            Error err = GetPosition(ignored, false);
            if (err != Error::None) {
                continue;
            }

            err = GetSpeed(ignored, false);
            if (err != Error::None) {
                continue;
            }

            // Set the ratio of encoder lines to actual units to be equal
            SetUnits(1, 1);

            initialized = true;
            return true;
        }
        while (ros::Time::now() - start < ros::Duration(2));
        ROS_ERROR("Kangaroo channel set a reset, but didn't appear to reset!");
        return false;
    }

    void ChannelRef::Set(
            KangarooBitpacked *position, 
            KangarooBitpacked *velocity,
            KangarooBitpacked *speedRamping,
            bool autoinit,
            bool relative, 
            bool noSpeedLimit, 
            bool raw)
    {
        if (autoinit && !EnsureInit())
            return; // There is no chance!

        CoprocessorMsg m;
        m.request.command = 36;
        m.request.data.push_back(this->channel);
        m.request.data.push_back((unsigned char)(noSpeedLimit ? 8 : 0 | raw ? 32 : 0));

        if (position) {
            m.request.data.push_back(1 | (relative ? 64 : 0));
            ChannelRef::PackNumber(m.request.data, *position);
        }
       
        if (velocity) {
            m.request.data.push_back(2 | (relative ? 64 : 0));
            ChannelRef::PackNumber(m.request.data, *velocity);
        }
        
        if (speedRamping) {
            m.request.data.push_back(3 | (relative ? 64 : 0));
            ChannelRef::PackNumber(m.request.data, *speedRamping);
        }

        m.request.reply = false;

        parent->SendData(m);
    }

    Error ChannelRef::GetParam(
            KangarooBitpacked &value,
            bool autoinit,
            int val,
            bool raw,
            bool relative)
    {
        if (autoinit && !EnsureInit())
            return Error::UnknownResponse;

        CoprocessorMsg m;
        m.request.command = 35;

        m.request.data.push_back(this->channel);
        m.request.data.push_back((unsigned char)(raw ? 32 : 0));
        m.request.data.push_back((unsigned char)(val + (relative ? 64 : 0)));
        m.request.reply = true;

        if (!parent->SendData(m)) {
            return Error::UnknownResponse;
        }

        if (m.response.data.size() < 3) {
            return Error::UnknownResponse;
        }
        
        if (m.response.data[0] != this->channel) {
            return Error::UnknownResponse;
        }

        auto flags = m.response.data[1];

        if (flags & 0x1)
        {
            Error e = static_cast<Error>(m.response.data[2]);
            CheckErrorForReset(e);
            return e;
        }

        value = 0;
        if (!UnpackNumber(m.response.data, 3, value))
            return Error::UnknownResponse;
        return Error::None;
    }

    Error ChannelRef::GetPosition(KangarooBitpacked &value,
            bool autoinit,
            bool raw,
            bool relative)
    {
        return GetParam(value, autoinit, 1, raw, relative);
    }
    
    Error ChannelRef::GetSpeed(KangarooBitpacked &value,
            bool autoinit,
            bool raw,
            bool relative)
    {
        return GetParam(value, autoinit, 2, raw, relative);
    }

    Error ChannelRef::GetMinPosition(KangarooBitpacked &value,
            bool autoinit,
            bool raw)
    {
        return GetParam(value, autoinit, 8, raw, false);
    }

    Error ChannelRef::GetMaxPosition(KangarooBitpacked &value,
            bool autoinit,
            bool raw)
    {
        return GetParam(value, autoinit, 9, raw, false);
    }
}
