#include "ros/ros.h"

#include "controller_manager/controller_manager.h"

#include "marsupial_driver/robothw.h"
#include "marsupial_driver/kangaroo.h"

#include "std_msgs/Bool.h"
#include "uc_msgs/CoprocessorStatus.h"

#include <time.h>

bool is_estopped = true;
bool is_go = false;

void status_cb(const uc_msgs::CoprocessorStatus::ConstPtr& s)
{
    is_estopped = !(s->software_estop && s->remote_estop && s->hard_estop);
}

// pls don't smite me c++ gods
// i know raii i promise
UCRobotHW *rbhw_ref;

void go_cb(const std_msgs::Bool::ConstPtr& s)
{
    is_go = s->data;
    if (!is_go)
    {
        rbhw_ref->halt();
    }
}

bool shouldIssueCommands() {
    return !is_estopped && is_go;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "kangaroo_node");

    ros::NodeHandle nh;
    ros::NodeHandle pnh("~");

    std::string rpcSrv;
    std::string commChannelName;
    double ticksPerRadian;
    std::string chanA;
    std::string chanB;

   
    if (!pnh.getParam("rpcSrv", rpcSrv))
        return -1;
    if (!pnh.getParam("commChannelName", commChannelName))
        commChannelName = "kangaroo";
    if (!pnh.getParam("ticksPerRadian", ticksPerRadian))
        ticksPerRadian = 159;
    if (!pnh.getParam("channelA", chanA))
        chanA = "1";
    if (!pnh.getParam("channelB", chanB))
        chanB = "2";

    ros::ServiceClient c = nh.serviceClient<uc_msgs::CoprocessorMsg>(rpcSrv);

    kangaroo::Kangaroo k(c, commChannelName, {(unsigned char)chanA[0], (unsigned char)chanB[0]});
    
    UCRobotHW hw(pnh, &k, ticksPerRadian);

    rbhw_ref = &hw;
    
    ros::Subscriber s = nh.subscribe("/status", 10, status_cb);
    ros::Subscriber s2 = nh.subscribe("/go", 10, go_cb);

    controller_manager::ControllerManager cm(&hw);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    ros::Duration controlRate(0.1);

    while (ros::ok()) 
    {
        hw.read(ros::Time::now(), controlRate);
        cm.update(ros::Time::now(), controlRate);
        hw.write(ros::Time::now(), controlRate);
        controlRate.sleep();
    }
    spinner.stop();
}
