#include "marsupial_driver/robothw.h"

using namespace hardware_interface;
using namespace kangaroo;
using namespace ros;

UCRobotHW::UCRobotHW(ros::NodeHandle &nh, kangaroo::Kangaroo *k, double ticksPerRadian) 
{
    std::string joint0Name, joint1Name;

    this->k = k;
    this->ticksPerRadian = ticksPerRadian;

    if (!nh.getParam("joint0handle", joint0Name))
        joint0Name = "left";
    
    if (!nh.getParam("joint1handle", joint1Name))
        joint1Name = "right";

    for (int i = 0; i < 2; i++)
    {
        states[i].pos = 0;
        states[i].pos_offset = 0;
        states[i].vel = 0;
        states[i].eff = 0;
        states[i].velcmd = 0;
    }

    if (!nh.getParam("joint0invert", states[0].shouldInvert))
        states[0].shouldInvert = false;

    if (!nh.getParam("joint1invert", states[1].shouldInvert))
        states[1].shouldInvert = false;


    JointStateHandle state_handle_j0(joint0Name,
            &states[0].pos,
            &states[0].vel,
            &states[0].eff);
    
    JointStateHandle state_handle_j1(joint1Name,
            &states[1].pos,
            &states[1].vel,
            &states[1].eff);

    joint_state_interface.registerHandle(state_handle_j0);
    joint_state_interface.registerHandle(state_handle_j1);

    JointHandle vel_handle_j0(joint_state_interface.getHandle(joint0Name),
            &states[0].velcmd);
    JointHandle vel_handle_j1(joint_state_interface.getHandle(joint1Name),
            &states[1].velcmd);

    velocity_joint_interface.registerHandle(vel_handle_j0);
    velocity_joint_interface.registerHandle(vel_handle_j1);

    ROS_INFO("Registered L: %s R: %s", joint0Name.c_str(), joint1Name.c_str());

    registerInterface(&joint_state_interface);
    registerInterface(&velocity_joint_interface);
}

void
UCRobotHW::read(
        const ros::Time &time,
        const ros::Duration &period
        )
{
    if (!shouldIssueCommands()) {
        for (int i = 0; i < 2; i++) {
            states[i].eff = 0;
            states[i].vel = 0;
        }
        return;
    }

    for (int i = 0; i < 2; i++)
    {
        states[i].eff = 0;
        
        ChannelRef& c = k->Channel(i);
   
        KangarooBitpacked b;
        Error err = c.GetPosition(b, true, false);
        if (err != Error::None) 
        {
            ROS_ERROR_THROTTLE(100, "Error encountered while attempting to retrieve position: %s", MessageForError(err)->c_str());
        }

        double s = (double)b / this->ticksPerRadian;
        double y = s - states[i].pos_offset;
        states[i].pos_offset = s;
      
        if (states[i].shouldInvert)
            states[i].pos -= y;
        else
            states[i].pos += y;

        err = c.GetSpeed(b, true, false);
        if (err != Error::None) 
        {
            ROS_ERROR_THROTTLE(100, "Error encountered while attempting to retrieve velocity: %s", MessageForError(err)->c_str());
        }

        states[i].vel = (double)b / this->ticksPerRadian;

        if (states[i].shouldInvert)
            states[i].vel = -states[i].vel;
    }
}

void
UCRobotHW::write(
        const ros::Time &time,
        const ros::Duration &period
        )
{
    if (!shouldIssueCommands())
        return;

    for (int i = 0; i < 2; i++)
    {
        KangarooBitpacked ticks = (int)(states[i].velcmd * this->ticksPerRadian);
        if (states[i].shouldInvert)
            ticks = -ticks;

        k->Channel(i).Set(nullptr, &ticks, nullptr, false, false, true);
    }
}

void 
UCRobotHW::halt()
{
    for (int i = 0; i < 2; i++)
    {
        k->Channel(i).PowerDown();
    }
}
