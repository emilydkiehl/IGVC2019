#pragma once

#include <initializer_list>

#include <uc_msgs/CoprocessorMsg.h>
#include <ros/service_client.h>


namespace kangaroo {
    
    using KangarooBitpacked = int32_t;

    enum class Error : uint8_t {
        None = 0,
        NoStart = 1,
        Home = 2,
        Ctrl = 3,
        BadMode = 4,
        UnknownParam = 5,
        Timeout = 6,
        UnknownResponse = 7,
        ErrorCount
    };

    extern const std::string KANG_ERR_MSG[static_cast<uint8_t>(kangaroo::Error::ErrorCount) + 1];
    constexpr std::string const *MessageForError(Error err) {
        return static_cast<uint8_t>(err) >= static_cast<uint8_t>(Error::ErrorCount) ? 
            &KANG_ERR_MSG[static_cast<uint8_t>(Error::ErrorCount)] : 
            &KANG_ERR_MSG[static_cast<uint8_t>(err)];
    }

    using KangarooChannel = unsigned char;
    
    class Kangaroo;
    class ChannelRef
    {
    public:
        ChannelRef()
            : channel('1') {};

        void Start();
        void SetUnits(KangarooBitpacked desired_units, KangarooBitpacked machine_units);
        void Home();
        void PowerDown();
        void PowerDownAll();
        void Set(
                KangarooBitpacked *position, 
                KangarooBitpacked *velocity, 
                KangarooBitpacked *speedRamping,
                bool autoinit = true,
                bool relative = false, 
                bool noSpeedLimit = false, 
                bool raw = false);

        bool EnsureInit();

        Error GetPosition(KangarooBitpacked &value, bool autoinit = true, bool raw = false, bool relative = false);
        Error GetSpeed(KangarooBitpacked &value, bool autoinit = true, bool raw = false, bool relative = false);
        Error GetMinPosition(KangarooBitpacked &value, bool autoinit = true, bool raw = false);
        Error GetMaxPosition(KangarooBitpacked &value, bool autoinit = true, bool raw = false);
        
        void setChannel(KangarooChannel c) {this->channel = c; }
    private:
        
        void CheckErrorForReset(Error e) {
            if (e == Error::NoStart ||
                    e == Error::Home)
                initialized = false;
        }

        void System(uint8_t sys_cmd, const std::vector<KangarooBitpacked> &params);

        Error GetParam(KangarooBitpacked &value, bool autoinit, int val, bool raw, bool relative);

        template<typename C>
        static void PackNumber(C& c, KangarooBitpacked bp) {
            if (bp < 0) { bp = -bp; bp <<= 1; bp |= 1; }
            else        {           bp <<= 1;          }

            while (true)
            {
                c.push_back((bp & 0x3f) | (bp >= 0x40 ? 0x40 : 0x00));
                bp >>= 6; if (!bp) { break; }
            }
        }
        template<typename C>
        static bool UnpackNumber(C& c, int off, KangarooBitpacked& bp)
        {
            for (int i = off; i < c.size(); i++) 
            {
                bp |= (uint32_t)(c[i] << ((i - off) * 6));
                if (!(c[i] & 0x40)) {
                    bp = (bp & 0x1) ? -(bp >> 1) : (bp >> 1);
                    return true; 
                }
            }
            return false;
        }
    protected:
        friend Kangaroo;
        KangarooChannel channel;
        Kangaroo *parent = nullptr;
        bool initialized = false;
    };
    
    class Kangaroo {
    public:
        
        Kangaroo(ros::ServiceClient &rpcClient, std::string channelName, std::initializer_list<KangarooChannel> channelLabels)
            : rpcClient(rpcClient), channelName(channelName), channels() {
            int i = 0;
            for (auto c : channelLabels) {
                channels[i].parent = this;
                channels[i].setChannel(c);
                i++;
            }
        }
        ChannelRef &Channel(uint8_t channelNumber) {
            return channels[channelNumber];
        }

        bool SendData(uc_msgs::CoprocessorMsg &msg);
    private:
        ros::ServiceClient &rpcClient;
        std::string channelName;
        std::array<ChannelRef, 2> channels;
    };
}
