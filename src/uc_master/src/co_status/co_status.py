#!/usr/bin/env python2
import rospy
import struct
from uc_msgs.srv import CoprocessorMsg, CoprocessorMsgRequest
from uc_msgs.msg import CoprocessorStatus

def grab_service():
    rospy.loginfo("Waiting for Coprocessor service...")
    try:
        rospy.wait_for_service("coprocessor", timeout=5)
    except:
        rospy.logwarn("Coprocessor exceeded startup time... will try again.")
        return None

    return rospy.ServiceProxy("coprocessor", CoprocessorMsg, persistent=True)

def must_grab_service():
    while not rospy.is_shutdown():
        svc = grab_service()
        if svc is not None:
            return svc
    raise Exception("Shutdown triggerered while waiting for a service")

def main():
    rospy.init_node("co_status", anonymous=False)
    pub = rospy.Publisher("status", CoprocessorStatus)

    svc = must_grab_service()
    delay = rospy.Rate(10)
    
    req = CoprocessorMsgRequest(
            channel="status",
            command=0,
            data=[],
            reply=True)

    while not rospy.is_shutdown():
        try:
            response = svc.call(req)
            
            estop, is_valid, is_enabled, ch0, ch1 = \
                struct.unpack("BBBBB", response.data)

            status = CoprocessorStatus()
            status.software_estop = estop & 0x1 == 0x1
            status.remote_estop = estop & 0x2 == 0x2
            status.hard_estop = estop & 0x4 == 0x4

            status.spek_is_valid = is_valid > 0
            status.spek_is_enabled = is_enabled > 0
            status.ch0 = ch0
            status.ch1 = ch1
        
            pub.publish(status)
        except rospy.ServiceException as exc:
            rospy.logerr("Coprocessor service errored... %s", str(exc))
            # Regrab service
            svc = must_grab_service()
            continue

        delay.sleep() 

if __name__ == "__main__":
    main()
