#include "ros/ros.h"
#include <ros/service_client.h>
#include <uc_msgs/CoprocessorMsg.h>

#include <stdio.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>

int16_t crc14(const uint8_t* data, size_t length)
{
      uint16_t crc = 0x3fff; 
      size_t i, bit;
      for(i = 0; i < length; i ++) {
        crc ^= data[i] & 0x7f;
        for(bit = 0; bit < 7; bit ++) {
            if(crc & 1) { 
                crc >>= 1; 
                crc ^= 0x22f0; 
            } else{ 
                crc >>= 1;                
            }
        }
      }
      return crc ^ 0x3fff;
}

size_t writeKangarooCommand(uint8_t address, uint8_t command,
        const uint8_t* data, uint8_t length, uint8_t* buffer) 
{
    size_t i; uint16_t crc;                                             
    buffer[0] = address | 0x80;
    buffer[1] = command;                     
    buffer[2] = length;
    for(i = 0; i < length; i ++) {                             
        buffer[3 + i] = data[i];                                 
    }                                                          
    crc = crc14(buffer, 3 + length);                           
    buffer[3 + length] =  crc & 0x7f;           
    buffer[4 + length] = (crc >> 7) & 0x7f; 
    return 5 + length;
}      

ssize_t mustRead(int fd, uint8_t *buf, size_t len)
{
    fd_set set;
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 100000;
    FD_ZERO(&set);
    FD_SET(fd, &set);

    uint8_t *bptr = buf;
    ssize_t last_read;
    while ((size_t)(bptr - buf) < len)
    {
        int rv = select(fd + 1, &set, NULL, NULL, &timeout);
        if (rv == -1)
        {
            ROS_ERROR("Response could not be read: %s", strerror(errno));
            return -1;
        }
        else if (rv == 0)
        {
            ROS_ERROR("Response could not be read: timeout");
            return -1;
        }
        else if ((last_read = read(fd, bptr, len - (bptr - buf))) == -1)
        {
            ROS_ERROR("Response could not be read: %s", strerror(errno));
            return -1;
        }
        else
        {
            bptr += last_read;
        }
    }
    return bptr - buf;
}

// Hello janky code!
#define FAULT(...) ROS_ERROR(__VA_ARGS__); ros::shutdown(); exit(-1)
class KangarooInstance {
private:
    uint8_t buf[255 + 5];
    int spfd;

public:
    KangarooInstance(std::string &port) 
    {
        spfd = open(port.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
        if (spfd == -1) {
            FAULT("Could not open serial port: %s", strerror(errno));
        }

        ROS_INFO("Opened port %s (%d)", port.c_str(), spfd);

        fcntl(spfd, F_SETFL, 0);

        struct termios fd_options;

        tcgetattr(spfd, &fd_options);
        
        fd_options.c_cflag |= ( CREAD | CLOCAL | CS8 );
        fd_options.c_cflag &= ~( PARODD | CRTSCTS | CSTOPB | PARENB );
        //fd_options.c_iflag |= ( );
        fd_options.c_iflag &= ~( IUCLC | IXANY | IMAXBEL | IXON | IXOFF | IUTF8 | ICRNL | INPCK );
        fd_options.c_oflag |= ( NL0 | CR0 | TAB0 | BS0 | VT0 | FF0 );
        fd_options.c_oflag &= ~( OPOST | ONLCR | OLCUC | OCRNL | ONOCR | ONLRET | OFILL | OFDEL | NL1 | CR1 | CR2 | TAB3 | BS1 | VT1 | FF1 );
        fd_options.c_lflag |= ( NOFLSH );
        fd_options.c_lflag &= ~( ICANON | IEXTEN | TOSTOP | ISIG | ECHOPRT | ECHO | ECHOE | ECHOK | ECHOCTL | ECHOKE );
        fd_options.c_cc[VINTR] = 0x03;
        fd_options.c_cc[VQUIT] = 0x1C;
        fd_options.c_cc[VERASE] = 0x7F;
        fd_options.c_cc[VKILL] = 0x15;
        fd_options.c_cc[VEOF] = 0x04;
        fd_options.c_cc[VTIME] = 0x01;
        fd_options.c_cc[VMIN] = 0x00;
        fd_options.c_cc[VSWTC] = 0x00;
        fd_options.c_cc[VSTART] = 0x11;
        fd_options.c_cc[VSTOP] = 0x13;
        fd_options.c_cc[VSUSP] = 0x1A;
        fd_options.c_cc[VEOL] = 0x00;
        fd_options.c_cc[VREPRINT] = 0x12;
        fd_options.c_cc[VDISCARD] = 0x0F;
        fd_options.c_cc[VWERASE] = 0x17;
        fd_options.c_cc[VLNEXT] = 0x16;
        fd_options.c_cc[VEOL2] = 0x00;

        cfsetospeed(&fd_options, B38400);
        cfsetispeed(&fd_options, B38400);
        
        tcsetattr(spfd, TCSANOW, &fd_options);
    }

    ~KangarooInstance() 
    {
        close(spfd);
    }

    bool handle(uc_msgs::CoprocessorMsg::Request &req, 
            uc_msgs::CoprocessorMsg::Response &resp,
            uint8_t address)
    {
        size_t len = writeKangarooCommand(address, req.command, req.data.data(), req.data.size(), buf);

        if (write(spfd, buf, len) == -1) {
            ROS_ERROR("Write to serial port (%d) failed: %s", spfd, strerror(errno));
            return false;
        }

        if (req.reply) {
            if (mustRead(spfd, buf, 3) == -1)
            {
                return false;
            }
            
            uint8_t len = buf[2];

            if (mustRead(spfd, &buf[3], len + 2) == -1)
            {
                return false;
            }

            auto crc = crc14(buf, 3 + len);

            if (((crc & 0x7f) != buf[len + 3]) ||
                    (((crc >> 7) & 0x7f) != buf[len + 4])) {
                ROS_ERROR("CRC fault");
                return false;
            }

            resp.data = std::vector<uint8_t>(&buf[3], &buf[3 + len]);
            return true;
        } else {
            resp.data = std::vector<uint8_t>();
            return true;
        }
    }
};

class KangarooChannel {
private:
    std::shared_ptr<KangarooInstance> i;
    uint8_t channel;

public:
    KangarooChannel(std::shared_ptr<KangarooInstance> i, uint8_t channel)
        : i(i), channel(channel) {}

    bool handle(uc_msgs::CoprocessorMsg::Request &req,
            uc_msgs::CoprocessorMsg::Response &resp)
    {
        return i->handle(req, resp, channel);
    }
};

std::map<std::string, std::shared_ptr<KangarooInstance>> instancemap;
std::map<std::string, std::unique_ptr<KangarooChannel>> channelmap;

bool displayResponse(uc_msgs::CoprocessorMsg::Request &req,
                     uc_msgs::CoprocessorMsg::Response &resp)
{
    auto chan = channelmap.find(req.channel);
    if (chan == channelmap.end()) {
        ROS_ERROR("Invalid ROS channel sent: %s", req.channel.c_str());
        return false;
    }

    return chan->second->handle(req, resp);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "coprocessor");
    ros::NodeHandle n;
    ros::NodeHandle priv("~");

    // First load and populate the instances
    std::map<std::string, std::string> instancePortMap;
    if (!priv.getParam("instances", instancePortMap))
    {
        FAULT("Could not load instances");
    }

    for (auto it = instancePortMap.begin(); it != instancePortMap.end(); it++)
    {
        instancemap.emplace(it->first, std::make_shared<KangarooInstance>(it->second));
    }
    
    std::map<std::string, std::string> channelRefMap;
    if (!priv.getParam("channels", channelRefMap)) {
        FAULT("Could not load channels");
    }

    for (auto it = channelRefMap.begin(); it != channelRefMap.end(); it++)
    {
        auto delimiterPos = it->second.find_last_of(".");
        if (delimiterPos == std::string::npos)
        {
            FAULT("Channel reference '%s' could not be parsed correctly!", it->second.c_str());
        }

        auto instanceName = std::string(it->second, 0, delimiterPos);
        auto instanceRef = instancemap.find(instanceName);
        if (instanceRef == instancemap.end())
        {
            FAULT("Channel reference '%s' referred to an instance '%s' which does not exist", 
                    it->second.c_str(), instanceName.c_str());
        }

        int channel;
        auto channelStr = std::string(it->second, delimiterPos + 1);
        try {
            channel = std::stoi(channelStr);
        } catch (const std::invalid_argument &i) {
            FAULT("Channel reference '%s' referred to a channel id '%s' - this must be a number!",
                    it->second.c_str(), channelStr.c_str());
        }

        channelmap.emplace(it->first, std::make_unique<KangarooChannel>(instanceRef->second, channel));
    }

    ros::ServiceServer service = n.advertiseService("coprocessor", displayResponse);

    ros::spin();

    return 0;
}
