#!/usr/bin/env python2
import roslaunch
import roslib
import rospy

from uc_msgs.msg import CoprocessorStatus
from uc_msgs.srv import CoprocessorMsg, CoprocessorMsgRequest, S3Get, S3List, S3Set, S3GetResponse, S3ListResponse, S3SetResponse

from std_msgs.msg import Bool
from geometry_msgs.msg import Twist

class Color:
    RED = 0
    GREEN = 1
    BLUE = 2
    ORANGE = 3
    YELLOW = 4

class Style:
    SOLID = 0
    SLOW_BLINK = 1
    FAST_BLINK = 2
    IDLE_SEQUENCE = 3
    SLOW_SEQUENCE = 4
    FAST_SEQUENCE = 5

class BaseState(object):
    def __init__(self, is_mode):
        self.is_mode = is_mode

    def enter(self, s3, prev):
        pass

    def exit(self, s3):
        pass

    def on_mode_change(self, s3, old_mode, new_mode):
        s3.switch_to_raw(new_mode)

    def on_state(self, s3, state):
        pass

class EstoppableState(BaseState):
    def handle_estop_state(self, s3, state):
        if not state.remote_estop or \
            not state.hard_estop:
            s3.switch_to('estop')
            return False
        return True

class EstopState(BaseState):
    def enter(self, s3, prev):
        self.prev = prev
        s3.set_safety_status(True)
        s3.set_estopped(True)
        s3.set_leds(Color.RED, Style.FAST_BLINK)
        s3.set_drive_source(None)

    def on_mode_change(self, s3, old_mode, new_mode):
        # We won't move out of the estop state, but we can retarget for
        # the new state
        self.prev = new_mode

    def on_state(self, s3, state):
        # Wait until we are no longer estopped
        if state.remote_estop and \
            state.hard_estop:
            s3.switch_to_raw(self.prev)

class IdleState(BaseState):
    def enter(self, s3, prev):
        s3.set_safety_status(False)
        s3.set_estopped(True)
        s3.set_leds(Color.ORANGE, Style.IDLE_SEQUENCE)

class ManIdleState(BaseState):
    def enter(self, s3, prev):
        s3.set_safety_status(False)
        s3.set_estopped(True)
        s3.set_leds(Color.BLUE, Style.IDLE_SEQUENCE)
   
    def on_state(self, s3, state):
        if state.spek_is_valid and state.spek_is_enabled:
            s3.switch_to('manual')

class ManualState(EstoppableState):
    def __init__(self, is_mode, return_state='man-idle'):
        super(ManualState, self).__init__(is_mode)
        self.return_state = return_state

    def enter(self, s3, prev):
        s3.set_safety_status(True)
        s3.set_estopped(False)
        s3.set_leds(Color.GREEN, Style.SOLID)
        s3.set_drive_source("/joystick_output")
    
    def on_state(self, s3, state):
        if self.handle_estop_state(s3, state):
            if not state.spek_is_valid or not state.spek_is_enabled:
                s3.switch_to(self.return_state)

class AutoState(EstoppableState):
    def enter(self, s3, prev):
        s3.set_safety_status(True)
        s3.set_estopped(False)
        s3.set_leds(Color.GREEN, Style.FAST_BLINK)
        s3.set_drive_source("/move_base/move_base")

    def on_state(self, s3, state):
        if self.handle_estop_state(s3, state):
            if state.spek_is_valid and state.spek_is_enabled:
                s3.switch_to('manual-auto-override')

class IopState(EstoppableState):
    def enter(self, s3, prev):
        s3.set_safety_status(True)
        s3.set_estopped(False)
        s3.set_leds(Color.ORANGE, Style.FAST_BLINK)
        s3.set_drive_source("/iop_output")

    def on_state(self, s3, state):
        self.handle_estop_state(s3, state)

class State3:
    def __init__(self, states, start_mode):
        self.states = states

        self.valid_modes = [x for x in states if states[x].is_mode]
        if start_mode not in self.valid_modes:
            raise Exception("Invalid start_mode for passed states")

        self.active_state = self.states[start_mode]
        self.active_mode = start_mode

        self.drive_sources = {}

    def start(self):
        rospy.wait_for_service('coprocessor', timeout=120)
        self.co = rospy.ServiceProxy('coprocessor', CoprocessorMsg)

        self.drive_pub = rospy.Publisher('/diff_drive_controller/cmd_vel', Twist, queue_size=1)
        self.selected_drive = None

        self.add_drive_source('/joystick_output')
        self.add_drive_source('/move_base/move_base')
        self.add_drive_source('/iop_output')

        self.go_pub = rospy.Publisher('/go', Bool, queue_size=1)

        self.status_sub = rospy.Subscriber("/status", CoprocessorStatus, callback=lambda m: self.status_msg(m))

        self.active_state.enter(self, None)

    def add_drive_source(self, chan):
        def cb(msg):
            if self.selected_drive == chan:
                self.drive_pub.publish(msg)

        self.drive_sources[chan] = rospy.Subscriber(chan, Twist, cb)

    def switch_to(self, new_state_name):
        self.switch_to_raw(self.states[new_state_name])
    
    def switch_to_raw(self, new_state):
        prev_state = self.active_state
        prev_state.exit(self)

        self.active_state = new_state
        self.active_state.enter(self, prev_state)

    def set_drive_source(self, chan):
        if chan is not None and chan not in self.drive_sources:
            rospy.logwarn("Drive source selected %s was not preloaded", chan)
            self.add_drive_source(chan)

        self.selected_drive = chan


    def set_leds(self, color, style):
        req = CoprocessorMsgRequest(
                channel='led', 
                command=0,
                data=[color, style],
                reply=False)

        self.co(req)

    def set_estopped(self, estop):
        req = CoprocessorMsgRequest(
                channel='status', 
                command=1,
                data=[0 if estop else 1],
                reply=False)

        self.co(req)
        
        b = Bool()
        b.data = not estop

        self.go_pub.publish(b)
    
    def set_safety_status(self, safety):
        req = CoprocessorMsgRequest(
                channel='status', 
                command=2,
                data=[1 if safety else 0],
                reply=False)

        self.co(req)

    def list_modes(self, req):
        resp = S3ListResponse()
        resp.modes = self.valid_modes
        return resp
    
    def get_mode(self, req):
        resp = S3GetResponse()
        resp.mode = self.active_mode
        return resp
    
    def set_mode(self, req):
        resp = S3SetResponse()
        if req.mode not in self.valid_modes:
            resp.success = False
            resp.message = "Invalid mode"
        else:
            self.active_state.on_mode_change(self, self.active_mode, self.states[req.mode])
            
            self.active_mode = req.mode 

            resp.success = True
            resp.message = "Switched"

        return resp

    def status_msg(self, m):
        self.active_state.on_state(self, m)

def main():
    rospy.init_node('state3', anonymous=False)
    states = {
            'idle': IdleState(True),
            'man-idle': ManIdleState(False),
            'manual': ManualState(True),
            'estop': EstopState(False),
            'auto': AutoState(True),
            'manual-auto-override': ManualState(False, 'auto'),
            'iop': IopState(True)
    }
    s3 = State3(states, 'idle')
    
    rospy.Service('s3/get', S3Get, lambda r: s3.get_mode(r))
    rospy.Service('s3/list', S3List, lambda r: s3.list_modes(r))
    rospy.Service('s3/set', S3Set, lambda r: s3.set_mode(r))

    s3.start()
    rospy.spin()
    s3.set_leds(Color.GREEN, Style.SLOW_BLINK)

if __name__ == "__main__":
    main()
