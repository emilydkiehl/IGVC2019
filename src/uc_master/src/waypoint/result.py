class SuccessResult():
    def __init__(self):
        pass

    def log(self, command):
        rospy.loginfo("PASS: {}".format(command))

    def do_continue(self):
        return True

class TimeoutResult():
    def __init__(self, timeout, should_continue):
        self.timeout = timeout
        self.should_continue = should_continue

    def log(self, command):
        rospy.logwarn("TIMEOUT({}) ({}s): {}".format(
            "continue" if self.should_continue else "halt",
            self.timeout.to_sec(), command))
    
    def do_continue(self):
        return self.should_continue

class HaltResult():
    def __init__(self):
        pass

    def log(self, command):
        rospy.logerr("HALT: {}".format(command))

    def do_continue(self):
        return False

class AbortResult():
    def log(self, command):
        rospy.logerr("ABORT: {}".format(command))

    def do_continue(self):
        return False

class RejectedResult():
    def log(self, command):
        rospy.logerr("REJECTED: {}".format(command))

    def do_continue(self):
        return True

class FatalResult():
    def __init__(self, msg):
        self.msg = msg

    def log(self, command):
        rospy.logerr("FATAL: {} - {}".format(command, self.msg))

    def do_continue(self):
        return False
