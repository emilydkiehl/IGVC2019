#!/usr/bin/env python

import actionlib
import geometry_msgs.msg as geo
import rospy
import std_msgs.msg as std
import tf.transformations

MoveBaseAction = mb.MoveBaseAction
MoveBaseGoal = mb.MoveBaseGoal
GoalStatus = actionlib.simple_action_client.GoalStatus

STOP = Waypoint(0, 0, "base_link")



def create_waypoint_from_line(line):
    operands = line.split(',')
    if len(operands) < 2:
        raise Exception("Invalid waypoint entry '{}'".format(line))

    frame_id = "global_gps"
    if len(operands) == 3:
        frame_id = operands[2]

    x = float(operands[0])
    y = float(operands[1])
    if frame_id == "global_gps":
        return UTMWaypoint(x, y)
    else:
        return Waypoint(x, y, operands[2])

def read_waypoints(filename):
    with open(filename) as f:
        lines = f.readlines()

    commands = []
    for line in lines:
        line = line.strip()
        if line == "":
            continue
        if not line.startswith('#'):
            commands.append(line)

    return [create_waypoint_from_line(l) for l in commands]

STATUS_DICT = {
    GoalStatus.ABORTED: "Aborted",
    GoalStatus.ACTIVE: "Active",
    GoalStatus.LOST: "Lost",
    GoalStatus.PENDING: "Pending",
    GoalStatus.PREEMPTED: "Preempted",
    GoalStatus.PREEMPTING: "Preempting",
    GoalStatus.RECALLED: "Recalled",
    GoalStatus.RECALLING: "Recalling",
    GoalStatus.REJECTED: "Rejected",
    GoalStatus.SUCCEEDED: "Succeeded"
}

def run():
    rospy.init_node('waypoints_node', anonymous=True)
    
    rospy.loginfo("Waiting for move base")
    #while True:
        #try:
    client = actionlib.SimpleActionClient("/move_base", MoveBaseAction)
    if not client.wait_for_server(timeout=rospy.Duration(secs=5)):
        rospy.logwarn("timed out waiting for move base, trying again.")
        return
        #except:
        #    return
    
    waypoint_filename = rospy.get_param("~waypoints_file", None)
    if waypoint_filename is None:
        waypoints = []
        rospy.logwarn("No waypoint file given: doing nothing.")
    else:
        waypoints = read_waypoints(waypoint_filename)
        i = 1
        for waypoint in waypoints:
            rospy.logdebug("%s: %s", i, waypoint)
            i += 1
      
    i = 1
    for waypoint in waypoints:
        retries = 45
        while True:

    rospy.loginfo("Finished all goals.")
    rospy.spin()
   
if __name__ == "__main__":
    run() 

