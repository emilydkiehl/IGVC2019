import re
import rospy

class CommandException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg

_option_re = re.compile("^([a-zA-Z_\-0-9]+)=((?:\S|\\ )*)$")
def parameter_parse(option_names, parameters):
    keyed = {}
    positional = []
    for param in parameters:
        m = _option_re.match(param)
        if m is not None:
            key = m.group(1)
            value = m.group(2)
            if key in option_names:
                keyed[key] = value
                continue
        positional.append(param) 
    return (positional, keyed)

# https://youtu.be/fpZZQ2ov4lc
def _mutator(scale):
    def d(s):
        a = None
        try:
            a = float(s)
            if a < 0:
                raise CommandException("time duration was negative ({})".format(a))
        except ValueError:
            raise CommandException("time value was not a number ({})".format(s))

        scld = a * scale
        secs = int(scld)
        nsecs = int((scld - secs) * 1000000000)

        return rospy.Duration(secs, nsecs)
    return d

_TIME_MUTATORS = [
    ('ns', _mutator(0.000000001)),
    ('ms', _mutator(0.001)),
    ('t', _mutator(1/20)), # minecraft ticks
    ('s', _mutator(1)),
    ('m', _mutator(60)),
    ('h', _mutator(60 * 60)),
    ('d', _mutator(24 * 60 * 60))
]
def parse_ros_duration(s):
    for (suffix, mutator) in _TIME_MUTATORS:
        if not s.endswith(suffix):
            continue
    
        number = s[0:-len(suffix)]

        return mutator(number)
    raise CommandException("Unrecognized time format")

_BOOL_KEYS = [
    ("t", True),
    ("f", False),
    ("true", True),
    ("false", False),
    ("y", True),
    ("yes", True),
    ("n", False),
    ("no", False)
]

def parse_boolean(s):
    lc = s.lower()
    for (key, value) in _BOOL_KEYS:
        if s == key:
            return value
    raise CommandException("Unrecognized boolean ({})".format(s))

class Module():
    def command_names(self):
        return []

    def construct_command(self, command_name, parameters):
        return None

    def on_start(self):
        pass
    
    def on_quit(self):
        pass
