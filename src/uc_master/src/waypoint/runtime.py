import module

import importlib

class RuntimeModule():
    def __init__(self, runtime):
        self.runtime = runtime

    def command_names(self):
        return ["with", "include"]

    def construct_command(self, command_name, parameters):
        if command_name == "with":
            k, p = module.parameter_parse([], parameters)

            if len(p) != 1:
                raise module.CommandException("incorrect usage: with <module>")

            module_name = p[0]

            if self.runtime.module_loaded(module_name):
                return None

            mod_mod = None
            try:
                mod_mod = importlib.import_module(module_name)
            except ModuleNotFoundError:
                raise module.CommandException(
                        "module {} does not exist".format(module_name))
            if "__MODULE__" not in mod_mod:
                raise module.CommandException(
                        "module {} did not contain a __MODULE__ variable"
                        .format(module_name))

            self.runtime.load_module(mod_mod)
            return None

class RuntimeScript:
    def __init__(self, cmds):
        self.cmds = cmds

    def do(self):
        pass

class RuntimeBase():
    def __init__(self):
        self.modules = {}
        self.scripts = {}

class Runtime:
    def __init__(self):
        self.modules = {}
        self.cmd_map = {}
        self.scripts = {}

        # bootstrap!
        self.load_module(RuntimeModule(self))

    def module_loaded(self, name):
        return name in self.modules

    def load_module(self, module_name, module):
        if module_name in self.modules:
            return

        self.modules[module_name] = module

        for c in module.command_names():
            self.cmd_map[c] = module

    def invalidate_scripts(self):
        self.scripts = {}

    def create_script(self, 
    
