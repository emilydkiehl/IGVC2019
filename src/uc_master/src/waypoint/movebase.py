import actionlib
import module
import move_base_msgs.msg as mb
import result
import rospy
import tf.transformations
import utm

MoveBaseAction = mb.MoveBaseAction
MoveBaseGoal = mb.MoveBaseGoal
GoalStatus = actionlib.simple_action_client.GoalStatus

INCOMPLETE_STATES = [
    GoalStatus.ACTIVE, GoalStatus.PENDING, GoalStatus.PREEMPTING, GoalStatus.RECALLING
]

def skeleton_move_goal():
    move_goal = MoveBaseGoal()
    move_goal.target_pose.header.stamp = rospy.Time.now()
    theta = 3.14
    quat = tf.transformations.quaternion_from_euler(0, 0, theta)
    quat_msg = geo.Quaternion(*quat)
    move_goal.target_pose.pose.orientation = quat_msg
    return move_goal

class Waypoint():
    def __init__(self, x, y, frame_id):
        self.x = x
        self.y = y
        self.frame_id = frame_id

    def __str__(self):
        return "Waypoint(frame_id={}, x={}, y={})".format(self.frame_id, 
                self.x, self.y)

    def get_goal(self):
        move_goal = skeleton_move_goal()
        move_goal.target_pose.header.frame_id = self.frame_id
        move_goal.target_pose.pose.position.x = self.x
        move_goal.target_pose.pose.position.y = self.y
        return move_goal

class UTMWaypoint(Waypoint):
    def __init__(self, lat, lon):
        self.lat = lat
        self.lon = lon
        self.utm_data = utm.from_latlon(lat, lon)
        super(UTMWaypoint, self).__init__(self.utm_data[0], self.utm_data[1], "utm")

    def __str__(self):
        return "UTMWaypoint(lat={}, lon={})".format(self.lat, self.lon)

class GoCommand():
    def __init__(self, goal, client, abort_retries=0, timeout=None, timeout_fatal=True):
        self.goal = goal
        self.client = client

        self.abort_retries = abort_retries
        self.aborts_left = abort_retries

        self.timeout = timeout
        self.timeout_fatal = timeout_fatal

    def __str__(self):
        r = "Go "
        if self.abort_retries > 0:
            r += "({}/{} retries) ".format(self.aborts_left, self.abort_retries)

        if self.timeout is not None:
            r += "(timeout{} {}s) ".format(
                    "!" if self.timeout_fatal else "", self.timeout.to_sec())

            return "{} -> {}".format(r, self.goal)

    def do(self):
        while True:
            goal = self.goal.get_goal()
            self.client.send_goal(goal)
            if not self.client.wait_for_result(self.timeout):
                return result.TimeoutResult(self.timeout, not self.timeout_fatal)

            goal_state = self.client.get_state()

            if goal_state in INCOMPLETE_STATES:
                if rospy.is_shutdown():
                    # TODO: Actually handle this at the termination of the script on receipt of a halt
                    rospy.logwarn("Goal state appears to be incomplete, yet killed. I'll kill our current goal, because you killed me :(")
                    self.client.send_goal_and_wait(STOP.get_goal(), execute_timeout=rospy.Duration(secs=120))
                    return result.HaltResult()
            elif goal_state == GoalStatus.SUCCEEDED:
                return result.SuccessResult()
            elif goal_state == GoalStatus.PREEMPTED:
                return result.FatalResult("Our goal to move_base was preempted by another goal")
            elif goal_state == GoalStatus.RECALLED:
                return result.FatalResult("Our goal to move_base was recalled by another goal")
            elif goal_state == GoalStatus.ABORTED:
                if self.aborts_left <= 0:
                    return result.AbortResult()
                else:
                    rospy.logwarn("Goal was aborted. Retries remaining: %i", self.aborts_left)
                    self.aborts_left -= 1

            elif goal_state == GoalState.REJECTED:
                return result.RejectedResult()
            else:
                return result.FatalResult("move_base returned a goal code which was unhandled ({})".format(goal_state))

class MoveBaseModule(module.Module):
    def command_names(self):
        return ["go"]

    def _safe_float_parse(key, k):
        if key not in k:
            raise module.CommandException("{} undefined".format(key))
        try:
            return float(k[key])
        except ValueError:
            raise module.CommandException("{} ({}) is not a number"
                    .format(key, k[key]))

    def construct_command(self, command_name, parameters):
        if command_name == "go":
            p, k = module.parameter_parse(["timeout", "aborts"], parameters)
            needs_utm_translation = "lat_lon" in p

            goal = None

            if needs_utm_translation:
                lat = MoveBaseModule._safe_float_parse("lat", k)
                lon = MoveBaseModule._safe_float_parse("lon", k)

                try:
                    goal = UTMWaypoint(lat, lon)
                except utm.error.OutOfRangeError:
                    raise module.CommandException("lat/lon out of range ({}, {})"
                            .format(lat, lon))
            else:
                x = MoveBaseModule._safe_float_parse("x", k)
                y = MoveBaseModule._safe_float_parse("y", k)
               
                if "frame_id" not in k:
                    raise module.CommandException("frame_id undefined")

                frame_id = k["frame_id"]

                goal = Waypoint(x, y, frame_id)

            abort_retries = 0
            timeout = None
            timeout_fatal = True

            if "abort_retries" in k:
                try:
                    abort_retries = int(k[abort_retries])
                    if abort_retries < 0:
                        raise module.CommandException("abort_retries is negative ({})".format(k[abort_retries]))
                except ValueError:
                    raise module.CommandException("abort_retries is not a number ({})".format(k[abort_retries]))

            if "timeout" in k:
                timeout = module.parse_ros_duration(k["timeout"])

            if "timeout_fatal" in k:
                timeout_fatal = module.parse_ros_duration(k["timeout_fatal"])

            return GoCommand(goal, self.client, abort_retries, timeout, timeout_fatal)
        else:
            raise Exception("something really bad happened :(")

    def on_start(self):
        self.client = actionlib.SimpleActionClient("/move_base", MoveBaseAction)
        if not self.client.wait_for_server(timeout=rospy.Duration(secs=5)):
            raise Exception("move_base client was unresponsive")

    def on_quit(self):
        pass

__MODULE__ = MoveBaseModule()
