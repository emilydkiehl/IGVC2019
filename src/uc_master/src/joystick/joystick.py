#!/usr/bin/env python2
import rospy
import struct
from uc_msgs.msg import CoprocessorStatus
from geometry_msgs.msg import Twist

def dz_and_ctr(x):
    # lol we need a deadband on this thing
    x = 118 - x
#    if x > 0:
#        x -= 1 
#    elif x < 0:
#        x += 1
#    else:
#        x = 0
    return float(x)


def main():
    rospy.init_node("joystick", anonymous=False)
    pub = rospy.Publisher("joystick_output", Twist)

    last_message = rospy.Time.now()

    def do_pub(x, z):
        twist = Twist()
        twist.linear.x = x
        twist.angular.z = z

        pub.publish(twist)

    def reset_time():
        last_message = rospy.Time.now()

    def on_msg(cs):
        if cs.ch0 < 20 or cs.ch1 < 20 or cs.ch1 > 200 or cs.ch0 > 200:
            return

        if cs.spek_is_valid and cs.spek_is_enabled:
            do_pub(dz_and_ctr(cs.ch0) / 80, -(dz_and_ctr(cs.ch1) / 120))
        else:
            do_pub(0, 0)

        reset_time()


    def deadman(e):
        if rospy.Time.now() - last_message > rospy.Duration(secs=1):
            do_pub(0, 0)
        reset_time()
   
    rospy.Timer(rospy.Duration(secs=1), deadman)

    rospy.Subscriber("status", CoprocessorStatus, on_msg)
    rospy.spin()

if __name__ == "__main__":
    main()
