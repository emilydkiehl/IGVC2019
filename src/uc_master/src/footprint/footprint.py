#!/usr/bin/env python2
import math
import rospy

import geometry_msgs.msg as g
import std_msgs.msg as std

#               A
#             -----
#            /     \
#        B  /       \
#          /         \
#         /           \
#        /             \
#        |              |
#        |              |
#        | C    .       |
#        |      D       |
#        +----|   |-----+
#          E  |---|G
#               F

# You can shoot me later for hardcoding this stuff.

A = 0.2286
B = 0.508
C = 0.4572
D = 0.2032
E = 0.2159
F = 0.2286
G = 0.2286


E2 = E * 2 + F
RIGHT = E2 / 2
LEFT = -RIGHT

RINNER = F / 2
LINNER = -RINNER

PBOTTOM = -D + G

RIGHT_TOP = (A / 2)
LEFT_TOP = -(A / 2)
DIST = RIGHT - RIGHT_TOP
TOP = C - D + math.sqrt((B * B) - (DIST * DIST))

MIDDLE = C - D

polygon = g.Polygon()
def add_point(x, y):
    p = g.Point32()
    p.x = x
    p.y = y
    p.z = 0
    polygon.points.append(p)

add_point(-D, LEFT)
add_point(MIDDLE, LEFT)
add_point(TOP, LEFT_TOP)
add_point(TOP, RIGHT_TOP)
add_point(MIDDLE, RIGHT)
add_point(-D, RIGHT)
add_point(-D, RINNER)
add_point(PBOTTOM, RINNER)
add_point(PBOTTOM, LINNER)
add_point(-D, LINNER)

def main():
    rospy.init_node("footprint", anonymous=False)

    s = "["
    is_first = True
    for p in polygon.points:
        if not is_first:
            s += ","
        is_first = False
        s += "[{},{}]".format(p.x, p.y)
       
    rospy.loginfo("Calculated footprint: %s", s)

    pub = rospy.Publisher("footprint", g.Polygon, queue_size=10)
    pub2 = rospy.Publisher("footprint_stamped", g.PolygonStamped, queue_size=10)
    r = rospy.Rate(1)
    while not rospy.is_shutdown():
        pub.publish(polygon)
        st = g.PolygonStamped()
        st.polygon = polygon
        st.header = std.Header()
        st.header.stamp = rospy.Time.now()
        st.header.frame_id = "base_link"
        pub2.publish(st)
        r.sleep()

if __name__ == "__main__":
    main()
