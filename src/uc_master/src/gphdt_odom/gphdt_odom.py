#!/usr/bin/env python

import rospy
import novatel_gps_msgs.msg as novatel
import nav_msgs.msg as nav
import sensor_msgs.msg as sen
import math
import tf.transformations as tfm
import geometry_msgs.msg as geo

def run():
    topic = 'gphdt_odom_node'
    rospy.init_node(topic, anonymous=True, log_level=rospy.DEBUG)
    pub = rospy.Publisher("~gpsodom", nav.Odometry, queue_size=50)
    imupub = rospy.Publisher("~gpsimu", sen.Imu, queue_size=50)
    def cb(gphdt):
        # TODO : clean up the next 3 lines
        # lol past me fuck off
        if gphdt.heading == 0:
            return

        yaw = math.radians(((-gphdt.heading) + 180) % 360)

        odom = nav.Odometry()
        odom.header = gphdt.header
        odom.header.stamp = rospy.Time.now()
        odom.child_frame_id = "gps"
        odom.pose.pose.orientation = geo.Quaternion(*tfm.quaternion_from_euler(0, 0, yaw))
        pub.publish(odom)

        imu = sen.Imu()
        imu.header = gphdt.header
        imu.orientation = odom.pose.pose.orientation
        imu.orientation_covariance[8] = 0.001
        imupub.publish(imu)

    rospy.Subscriber("/gphdt", novatel.Gphdt, cb)
    rospy.spin()

if __name__ == "__main__":
    run()
