#include <novatel_gps_driver/parsers/gphdt.h>
#include <boost/make_shared.hpp>

const std::string novatel_gps_driver::GphdtParser::MESSAGE_NAME = "GPHDT";

uint32_t novatel_gps_driver::GphdtParser::GetMessageId() const
{
  return 0;
}

const std::string novatel_gps_driver::GphdtParser::GetMessageName() const
{
  return MESSAGE_NAME;
}

novatel_gps_msgs::GphdtPtr novatel_gps_driver::GphdtParser::ParseAscii(const novatel_gps_driver::NmeaSentence& sentence) throw(ParseException)
{
  const size_t MIN_LENGTH = 3;

  // Check that the message is at least as long as a a GPHDT with no degrees
  if (sentence.body.size() < MIN_LENGTH)
  {
    std::stringstream error;
    error << "Expected GPHDT length >= " << MIN_LENGTH
          << ", actual length = " << sentence.body.size();
    throw ParseException(error.str());
  }

  novatel_gps_msgs::GphdtPtr msg = boost::make_shared<novatel_gps_msgs::Gphdt>();
  msg->message_id = sentence.body[0];

  bool valid = true;

  double heading = 0.0;
  valid = valid && ParseDouble(sentence.body[1], heading);

  if (valid)
  {
    msg->heading = heading;
  }
  
  std::string degrees_true = sentence.body[2];
  if(!degrees_true.empty())
  {
      msg->degrees_true = degrees_true;
  }


  return msg;
}
