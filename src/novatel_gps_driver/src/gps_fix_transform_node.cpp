#include "ros/ros.h"

#include "gps_common/GPSFix.h"
#include "gps_common/conversions.h"
#include "novatel_gps_msgs/Gphdt.h"

#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "tf2_ros/transform_broadcaster.h"
#include "tf2_ros/transform_listener.h"
#include "tf2_ros/buffer.h"

double last_heading = 0;
tf2_ros::Buffer *tfBuffer;
tf2_ros::TransformListener *tfListener;
tf2_ros::TransformBroadcaster *utm_broadcaster;

void updateTranslation(double easting, double northing, ros::Time time)
{
    geometry_msgs::TransformStamped mapToGpsStamped;
    mapToGpsStamped = tfBuffer->lookupTransform("gps", "map", time);

    tf2::Transform mapToGps;
    tf2::convert(mapToGpsStamped.transform, mapToGps);

    tf2::Vector3 gpsToUtmV(easting, northing, 0);

    tf2::Quaternion gpsToUtmQ;
    gpsToUtmQ.setRPY(0, 0, last_heading);

    tf2::Transform gpsToUtm;
    gpsToUtm.setOrigin(gpsToUtmV);
    gpsToUtm.setRotation(gpsToUtmQ);

    tf2::Transform mapToUtm;
    mapToUtm = mapToGps * gpsToUtm;

    geometry_msgs::TransformStamped t;
    t.header.stamp = time;
    t.header.frame_id = "map";
    t.child_frame_id = "utm";
    t.transform = tf2::toMsg(mapToUtm);

    // sanity check
    //t.transform.translation.z = 0;

    utm_broadcaster->sendTransform(t);
}

void gpsCallback(const gps_common::GPSFix& msg)
{
    double northing, easting;
    std::string zone;
    gps_common::LLtoUTM(msg.latitude, msg.longitude, northing, easting, zone);
    updateTranslation(easting, northing, msg.header.stamp);
}

void gphdtCallback(const novatel_gps_msgs::Gphdt& msg)
{
    last_heading = (360 - msg.heading) * 3.1415 / 180;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "gps_fix_transform_node");
    ros::NodeHandle n;

    utm_broadcaster = new tf2_ros::TransformBroadcaster;
    tfBuffer = new tf2_ros::Buffer;
    tfListener = new tf2_ros::TransformListener(*tfBuffer);

    ros::Subscriber a = n.subscribe("/gps", 1, gpsCallback);
    ros::Subscriber b = n.subscribe("/gphdt", 1, gphdtCallback);

    ros::spin();
    return 0;
}
