#!/usr/bin/env python

import rospy
import utm
#import geodesy

import tf

from nav_msgs.msg import Odometry
from gps_common.msg import GPSFix
from novatel_gps_msgs.msg import Gphdt
from geometry_msgs.msg import PoseWithCovarianceStamped

pub = None
last_heading = 0

def gps_callback(data):

    easting, northing, zone, zone_num = utm.from_latlon(data.latitude, data.longitude)
    """
    t = geometry_msgs.msg.TransformStamped()
    t.header.stamp = rospy.Time.now()
    t.frame_id = "gps"
    t.child_frame_id = "utm"
    t.transform.translation.x = easting
    t.transform.translation.y = northing
    t.transform.translation.z = 0.0
    tf.transformations.quaternion_about_axis(last_heading * 3.14 / 180,  (0,0,1))
    t.transform.rotation.x = q[0]
    t.transform.rotation.y = q[1]
    t.transform.rotation.z = q[2]
    t.transform.rotation.w = q[3]
    """

    br = tf.TransformBroadcaster()
    #br.sendTransform(t)

    # tf.transformations.quaternion_inverse(tf.transformations.quaternion_about_axis(last_heading * 3.14 / 180,  (0,0,1)))
    br.sendTransform( (easting * -1, northing * -1, 0),
        tf.transformations.quaternion_about_axis(last_heading * 3.14 / 180,  (0,0,1)),
        rospy.Time.now(),
        "utm",
        "gps")


def heading_callback(data):
    global last_heading
    last_heading = data.heading

def listener():
    rospy.init_node('gps_pose', anonymous=True)

    pub = rospy.Publisher('gps_pose', PoseWithCovarianceStamped, queue_size=10)

    rospy.Subscriber("/gps", GPSFix, gps_callback)
    rospy.Subscriber("/gphdt", Gphdt, heading_callback)

    rospy.spin()

if __name__ == '__main__':
    listener()
