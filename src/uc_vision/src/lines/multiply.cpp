#include "multiply.h"

#include <limits>

#include "opencv2/cvconfig.h"

#include "opencv2/cudaarithm.hpp"
#include "opencv2/core/utility.hpp"

#include "opencv2/core/cvdef.h"
#include "opencv2/core/base.hpp"
#include "opencv2/core/cuda.hpp"

/*NOTE: This is directly from opencv source code. Hate yourself? cool me too. */
namespace cv { namespace cuda {
    CV_EXPORTS cv::String getNppErrorMessage(int code);
    CV_EXPORTS cv::String getCudaDriverApiErrorMessage(int code);

    CV_EXPORTS GpuMat getInputMat(InputArray _src, Stream& stream);

    CV_EXPORTS GpuMat getOutputMat(OutputArray _dst, int rows, int cols, int type, Stream& stream);
    static inline GpuMat getOutputMat(OutputArray _dst, Size size, int type, Stream& stream)
    {
        return getOutputMat(_dst, size.height, size.width, type, stream);
    }

    CV_EXPORTS void syncOutput(const GpuMat& dst, OutputArray _dst, Stream& stream);
}}

void mulMatSpecial(const cv::cuda::GpuMat& src, const cv::cuda::GpuMat& src2, cv::cuda::GpuMat& dst, cv::cuda::Stream& stream);

// A special function that multiplies together src1 and src2 (CV_8U), then dividing them by 256.
void multiplySpecial(cv::InputArray src1, cv::InputArray src2, cv::OutputArray dst)
{
    cv::cuda::Stream stream;
    cv::cuda::GpuMat gsrc1, gsrc2, gdst;
    gsrc1 = getInputMat(src1, stream);
    gsrc2 = getInputMat(src2, stream);
    gdst = getOutputMat(dst, src1.size(), CV_8U, stream);
    mulMatSpecial(gsrc1, gsrc2, gdst, stream);
    syncOutput(gdst, dst, stream);
}
