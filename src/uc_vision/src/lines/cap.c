#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stropts.h>
#include <unistd.h>

#include <linux/v4l2-subdev.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "cap.h"

static int xioctl(int fh, unsigned long request, void *arg)
{
    int r;

    do {
        r = ioctl(fh, request, arg);
    } while (-1 == r && EINTR == errno);

    return r;
}

struct dev_buf {
    void *start;
    __u32 length;
} *buffers;

struct dev
{
    int fd;
    struct dev_buf *buffers;
    __u32 count;
};

static int init_mmap(struct dev *d)
{
    struct v4l2_requestbuffers req;

    memset(&req, 0, sizeof(struct v4l2_requestbuffers));
    req.count = 4;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(d->fd, VIDIOC_REQBUFS, &req))
        return -1;

    if (req.count < 1)
        return -1;
    
    d->buffers = (struct dev_buf *)calloc(req.count, sizeof(*d->buffers));
    if (!d->buffers)
        return -1;

    struct v4l2_buffer buf;
    for (d->count = 0; d->count < req.count; ++d->count)
    {
        memset(&buf, 0, sizeof(buf));
        buf.type = req.type;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = d->count;
        
        if (-1 == xioctl(d->fd, VIDIOC_QUERYBUF, &buf))
            goto FREE_EXIT;

        d->buffers[d->count].length = buf.length;
        d->buffers[d->count].start =
            mmap(NULL,
                    buf.length,
                    PROT_READ| PROT_WRITE,
                    MAP_SHARED,
                    d->fd, buf.m.offset);

        if (MAP_FAILED == d->buffers[d->count].start)
        {
            goto FREE_EXIT;
        }

        continue;
FREE_EXIT:
        while (d->count > 0)
        {
            munmap(d->buffers[d->count - 1].start,
                    d->buffers[d->count - 1].length);
            d->count--;
        }

        free(d->buffers);
        return -1;
    }
    return 0;
}

int start_capturing(void *d)
{
    for (size_t i = 0; i < ((struct dev*)d)->count; i++)
    {
        struct v4l2_buffer buf;
        memset(&buf, 0, sizeof(buf));
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;
        if (-1 == xioctl(((struct dev*)d)->fd, VIDIOC_QBUF, &buf))
	    return -1;	

    }

    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == xioctl(((struct dev*)d)->fd, VIDIOC_STREAMON, &type))
        return -1;
    return 0;
}

int stop_capturing(void *d)
{
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == xioctl(((struct dev*)d)->fd, VIDIOC_STREAMOFF, &type))
        return -1;

    struct v4l2_requestbuffers req;
    memset(&req, 0, sizeof(req));
    req.count = 0;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(((struct dev*)d)->fd, VIDIOC_REQBUFS, &req))
        return -1;
    return 0;
}

static int init_device(struct dev *d, __u32 width, 
        __u32 height, __u32 format, __u32 fps)
{
    struct v4l2_capability cap;
    struct v4l2_cropcap cropcap;
    struct v4l2_crop crop;
    struct v4l2_format fmt;
    struct v4l2_subdev_frame_interval sdi;

    if (-1 == xioctl(d->fd, VIDIOC_QUERYCAP, &cap))
        return -1;

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
        return -1;

    memset(&cropcap, 0, sizeof(cropcap));
    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (0 == xioctl(d->fd, VIDIOC_CROPCAP, &cropcap))
    {
        crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect;

        xioctl(d->fd, VIDIOC_S_CROP, &crop);
    }

    memset(&fmt, 0, sizeof(fmt));
    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    fmt.fmt.pix.width = width;
    fmt.fmt.pix.height = height;
    fmt.fmt.pix.pixelformat = format;
    fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;

    if (-1 == xioctl(d->fd, VIDIOC_S_FMT, &fmt))
        return -1;

    /*memset(&sdi, 0, sizeof(sdi));
    sdi.interval.numerator = 1;
    sdi.interval.denominator = fps;

    if (-1 == xioctl(d->fd, VIDIOC_S_FMT, &sdi))
        return -1;*/

    return init_mmap(d);
}

static int uninit_device(struct dev* d)
{
    for (; d->count >= 0; --d->count)
    {
        if (!munmap(d->buffers[d->count - 1].start,
                d->buffers[d->count - 1].length))
            return -1;
    }
    return 0;
}

static int close_device(struct dev* d)
{
    if (d->fd == -1)
        return -1;

    if (close(d->fd) == -1)
        return -1;

    d->fd = -1;
    return 0;
}

static int open_device(struct dev* d, const char *dev_name)
{
    struct stat st;
    if (-1 == stat(dev_name, &st))
        return -1;
    
    if (!S_ISCHR(st.st_mode))
        return -1;

    d->fd = open(dev_name, O_RDWR | O_NONBLOCK, 0);

    if (-1 == d->fd)
        return -1;
    
    return 0;
}

int init_camera(void **handle, const char *devname, __u32 width, __u32 height, 
        __u32 format, __u32 fps)
{
    *handle = (void*)malloc(sizeof(struct dev));

    int r;

    if ((r = open_device((struct dev*)*handle, devname)))
        return r;
    r = init_device((struct dev*)*handle, width, height, format, fps);
    return r;
}

int free_camera(void *handle)
{
    if (stop_capturing((struct dev*) handle))
        return -1;

    if (uninit_device((struct dev*) handle))
        return -1;

    return close_device((struct dev*) handle);
}

int get_frame(struct handleref *handles, size_t handn, 
        struct cam_frame **frames, int *size)
{
    if (handn == 0)
    {
        *size = 0;
        return 0;
    }

    fd_set fds;
    
    FD_ZERO(&fds);
    int maxfd = -1;
    for (size_t hn = 0; hn < handn; hn++)
    {
        int fd = ((struct dev*)handles[hn].handle)->fd; 
        FD_SET(fd, &fds);
        if (fd > maxfd)
            maxfd = fd;
    }

    struct timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 0;

    int ssize = select(maxfd + 1, &fds, NULL, NULL, &tv);

    if (ssize == 0)
    {
        *size = 0;
        return 0; // TIMEOUT
    }

    if (ssize < *size)
        *size = ssize;

    for (int i = 0; i < *size; i++)
    {
        frames[i] = (struct cam_frame*)malloc(sizeof(struct cam_frame));
        struct cam_frame *cf = frames[i];
        cf->handle = NULL;
        for (size_t hn = 0; hn < handn; hn ++)
        {
            if (FD_ISSET(((struct dev*)handles[hn].handle)->fd, &fds))
            {
                cf->handle = (struct handleref*)(&handles[hn]);
                break;
            }
        }

        if (cf->handle == NULL)
            // something very bad happened that we didn't expect.
            return -1;

        memset(&cf->b, 0, sizeof(cf->b));

        cf->b.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

        if (-1 == xioctl(((struct dev*)(cf->handle->handle))->fd,
                    VIDIOC_DQBUF, &cf->b))
            return -1;

        cf->data = ((struct dev*)(cf->handle->handle))->buffers[cf->b.index].start;
        cf->length = ((struct dev*)(cf->handle->handle))->buffers[cf->b.index].length;
    }
    return 0;
}

int release_frame(struct cam_frame *frame)
{
    int fd = ((struct dev*)frame->handle->handle)->fd;
    return xioctl(fd, VIDIOC_QBUF, &frame->b);
}
