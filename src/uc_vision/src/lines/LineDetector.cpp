#include "LineDetector.h"

#include "multiply.h"

#include <chrono>

void LineDetector::populateKernels()
{
    float horizontal_mat[9][9] = {  
                            {-1, -1, -1, -1, -1, -1, -1, -1, -1},
                            {-1, -1, -1, -1, -1, -1, -1, -1, -1},
                            {-1, -1, -1, -1, -1, -1, -1, -1, -1},
                            { 1,  1,  1,  1,  1,  1,  1,  1,  1},
                            { 1,  1,  1,  1,  1,  1,  1,  1,  1},
                            { 1,  1,  1,  1,  1,  1,  1,  1,  1},
                            { 0,  0,  0,  0,  0,  0,  0,  0,  0},
                            { 0,  0,  0,  0,  0,  0,  0,  0,  0}, 
                            { 0,  0,  0,  0,  0,  0,  0,  0,  0}};

    float diagonal_mat[9][9] = {
                          {-1, -1, -1, -1, -1, -1, -1, -1, -1},
                          {-1, -1, -1, -1, -1, -1, -1,  0,  1},
                          {-1, -1, -1, -1, -1,  0,  1,  1,  1},
                          {-1, -1, -1,  0,  1,  1,  1,  1,  1},
                          {-1,  0,  1,  1,  1,  1,  1,0.5,  0},
                          { 1,  1,  1,  1,  1,0.5,  0,  0,  0},
                          { 1,  1,  1,0.5,  0,  0,  0,  0,  0},
                          { 1,0.5,  0,  0,  0,  0,  0,  0,  0},
                          { 0,  0,  0,  0,  0,  0,  0,  0,  0}};

    float diagonal_mat2[9][9] = {  
                       { -0.89, -0.89, -0.89, -0.89, -0.89, -0.89, -0.89,     1,     1},
			           { -0.89, -0.89, -0.89, -0.89, -0.89, -0.89,     1,     1,     1},
			           { -0.89, -0.89, -0.89, -0.89, -0.89,     1,     1,     1,     0}, 
			           { -0.89, -0.89, -0.89, -0.89,     1,     1,     1,     0,     0}, 
			           { -0.89, -0.89, -0.89,     1,     1,     1,     0,     0,     0}, 
			           { -0.89, -0.89,     1,     1,     1,     0,     0,     0,     0}, 
			           { -0.89,     1,     1,     1,     0,     0,     0,     0,     0}, 
			           {     1,     1,     1,     0,     0,     0,     0,     0,     0}, 
			           {     1,     1,     0,     0,     0,     0,     0,     0,     0}}; 


    cv::Mat cpu_h_m(9, 9, CV_32FC1, horizontal_mat);
    cv::Mat cpu_d_m(9, 9, CV_32FC1, diagonal_mat);
    cv::Mat cpu_d_m2(9, 9, CV_32FC1, diagonal_mat2);

    cv::Mat softened_h_m;
    cv::Mat softened_d_m;
    cv::Mat softened_d_m2;
    
    cv::divide(cpu_h_m, 27.0, softened_h_m);
    cv::divide(cpu_d_m, 25.0, softened_d_m);
    cv::divide(cpu_d_m2, 25.0, softened_d_m2);
    
    cv::Mat trans_soft_h_m;
    cv::Mat trans_soft_d_m;

    cv::transpose(softened_h_m, trans_soft_h_m);
    cv::transpose(softened_d_m, trans_soft_d_m);
    
    cv::Mat flipped_trans_soft_h_m;
    cv::Mat flipped_trans_soft_d_m;
    cv::Mat flipped_soft_d_m2;
    
    cv::flip(trans_soft_h_m, flipped_trans_soft_h_m, 0);
    cv::flip(trans_soft_d_m, flipped_trans_soft_d_m, 0);
    cv::flip(softened_d_m2, flipped_soft_d_m2, 0);

#define K(k) {kernels.push_back(cv::cuda::createLinearFilter(CV_8U, CV_8U, k)); cv::Mat temp; cv::flip(k, temp, 0); comp_kernels.push_back(cv::cuda::createLinearFilter(CV_8U, CV_8U, temp));}

    K(softened_h_m);
    K(softened_d_m);
    K(softened_d_m2);
    K(trans_soft_h_m);
    K(trans_soft_d_m);
    K(flipped_trans_soft_h_m);
    K(flipped_trans_soft_d_m);
    K(flipped_soft_d_m2);
}

using namespace std::chrono;
void LineDetector::processImage(cv::Mat m)
{
    if (m.empty())
    {
        ROS_ERROR("Image passed is empty.");
        return;
    }

    cv::cuda::GpuMat input_image(m);
    
    if (needs_resize)
    {
        cv::cuda::GpuMat resized_image;
        cv::cuda::resize(input_image, resized_image, cv::Size(), resize_ratio, resize_ratio);
        input_image = resized_image;
    }

    cv::cuda::GpuMat hsv_input;
    cv::cuda::cvtColor(input_image, hsv_input, cv::COLOR_BGR2HSV);

    cv::cuda::GpuMat hsv_split[3];
    cv::cuda::split(hsv_input, hsv_split);

    cv::cuda::GpuMat s_blur;
    cv::cuda::GpuMat v_blur;

    median_blur_filter->apply(hsv_split[1], s_blur);
    median_blur_filter->apply(hsv_split[2], v_blur);
    
    cv::cuda::GpuMat inverted_s_blur;
    cv::cuda::subtract(255, s_blur, inverted_s_blur);

    cv::cuda::GpuMat s_blur_v_blur_product;
    multiplySpecial(s_blur, v_blur, s_blur_v_blur_product);

    auto kitr = kernels.begin();
    auto ckitr = comp_kernels.begin();

    cv::cuda::GpuMat working_image(s_blur.size(), CV_8U, 0.0);
    cv::cuda::GpuMat ker_f2d_out, cker_f2d_out, kercker_m, thresh_i;
    for (; kitr != kernels.end() && ckitr != comp_kernels.end(); kitr++, ckitr++)
    {
        (*kitr)->apply(s_blur_v_blur_product, ker_f2d_out);
        (*ckitr)->apply(s_blur_v_blur_product, cker_f2d_out);
        multiplySpecial(ker_f2d_out, cker_f2d_out, kercker_m);

        cv::cuda::threshold(kercker_m, thresh_i, threshold_value, 255.0, CV_THRESH_BINARY);

        cv::cuda::bitwise_or(working_image, thresh_i, working_image);
    }

    cv::Mat gpu_output(working_image);
    
    cv::Mat thresh_out;
    cv::threshold(gpu_output, thresh_out, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
    
    std::vector<cv::Vec4i> hierarchy;
    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(thresh_out, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_TC89_L1, cv::Point(0, 0));

    auto cloud_in = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();

    for (auto contour = contours.begin(); contour != contours.end(); contour++)
    {
        auto area = cv::contourArea(*contour);
        if (area > this->min_area)
        {
            for (auto point = contour->begin(); point != contour->end(); point++)
            {
                float x_p = point->x*this->m[0][0] + point->y*this->m[0][1] + this->m[0][2];
                float y_p = point->x*this->m[1][0] + point->y*this->m[1][1] + this->m[1][2];
                float n   = point->x*this->m[2][0] + point->y*this->m[2][1] + this->m[2][2];
                
                cloud_in->points.push_back(pcl::PointXYZ(x_p / n, y_p / n, 0.0));
            }
        }
    }
    
    cloud_in->header.frame_id = this->frame_id;
    cloud_in->height = 1;
    cloud_in->width = cloud_in->points.size();

    pointcloudPublisher.publish(cloud_in);
}

void LineDetector::updateResize()
{
    if (!this->needs_resize)
    {
        memcpy(this->m, this->m_orig, sizeof(this->m_orig));
    } else
    {
        
        float scale[3][3];
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                scale[i][j] = 0;
            }
        }


        float scaler = 1 / this->resize_ratio;
        
        scale[0][0] = scaler;
        scale[1][1] = scaler;
        scale[2][2] = 1;

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                float sum = 0;
                for (int k = 0; k < 3; k++)
                {
                    // There is a better way to do all of this but i don't care rn
                    sum += this->m_orig[i][k] * scale[k][j];
                }
                m[i][j] = sum;
            }
        }
    }
}
