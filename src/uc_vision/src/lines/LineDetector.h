#pragma once
/**
 * IT IS I. JOE. THE TRUE CREATOR OF THE UNIVERSE
 *
 * joe was here may 30, 2019
 */
#include "ros/ros.h"

#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/core/cuda.hpp"
#include <opencv2/cudaarithm.hpp>
#include "opencv2/cudafilters.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"

#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include "sensor_msgs/Image.h"
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud.h>
#include "std_msgs/String.h"

#include <pcl_ros/point_cloud.h>
#include <pcl/common/transforms.h>
#include <pcl/conversions.h>
#include <pcl/point_types.h>

#include <dynamic_reconfigure/server.h>
#include <uc_vision/LineDetectorConfig.h>

#include <fstream>
#include <sstream>
#include <vector>
#include <array>


class LineDetector {
public:
    double resize_ratio;
    bool needs_resize;
    double min_area;
    int threshold_value;
    
    cv::Ptr<cv::cuda::Filter> median_blur_filter;
    std::vector<cv::Ptr<cv::cuda::Filter>> kernels;
    std::vector<cv::Ptr<cv::cuda::Filter>> comp_kernels;
    

    float m[3][3];
    float m_orig[3][3];
    std::string frame_id;
    std::string camera_feed;

    ros::Publisher pointcloudPublisher;

    void populateKernels();
    void processImage(cv::Mat m);

    void updateResize();
};
