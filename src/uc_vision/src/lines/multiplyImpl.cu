#include "opencv2/opencv_modules.hpp"
#include "opencv2/cudev.hpp"

using namespace cv::cudev;

template <typename ScalarDepth> struct TransformPolicy : DefaultTransformPolicy
{
};
template <> struct TransformPolicy<double> : DefaultTransformPolicy
{
	enum {
		shift = 1
	};
};


template <typename T, typename D> struct MulOp : binary_function<T, T, D>
{
	__device__ __forceinline__ D operator ()(T a, T b) const
	{
        return (D)((((ushort)a) * ((ushort)b)) >> 8);
	}
};

void mulMatSpecial(const GpuMat& src, const GpuMat& src2, GpuMat& dst, Stream& stream)
{

    GpuMat src1_ = src.reshape(1);
    GpuMat src2_ = src2.reshape(1);
    GpuMat dst_ = dst.reshape(1);

    MulOp<uchar, uchar> op;
    gridTransformBinary_<TransformPolicy<uchar>>(globPtr<uchar>(src1_), globPtr<uchar>(src2_), globPtr<uchar>(dst), op, stream);
}
