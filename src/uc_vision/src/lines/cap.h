#pragma once

#include <linux/videodev2.h>

#ifdef __cplusplus
extern "C" {
#endif

int init_camera(void **handle, const char *devname, __u32 width, 
        __u32 height, __u32 format, __u32 fps);

int free_camera(void *handle);

int start_capturing(void *handle);
int stop_capturing(void *handle);

struct handleref {
    void *handle;
    void *userdata;
};

struct cam_frame {
    struct handleref *handle;
    struct v4l2_buffer b;
    void *data;
    __u32 length;
};

int get_frame(struct handleref *handles, size_t handn, 
        struct cam_frame **frames, int *size);

int release_frame(struct cam_frame *frame);

#ifdef __cplusplus
}
#endif
