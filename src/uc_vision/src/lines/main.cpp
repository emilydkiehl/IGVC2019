#include "LineDetector.h"
#include "camera.h"

LineDetector detector;

// callback for dynamic reconfigure
void dynamicCallback(uc_vision::LineDetectorConfig &config, uint32_t level);

void subscriberCallback(const sensor_msgs::ImageConstPtr &msg)
{
    cv_bridge::CvImagePtr img = cv_bridge::toCvCopy(msg, "bgr8");
    detector.processImage(img->image);
}

void dynamicCallback(uc_vision::LineDetectorConfig &config, uint32_t level) {
    ROS_INFO("Reconfigure Request: \nresize_ratio:\t%f\nblur_kernal_size:\t%d\nthreshold_value\t%d\nmin_area\t%f\n",
            config.resize_ratio,
            config.blur_kernal_size,
            config.threshold_value,
            config.min_area);

    if (config.blur_kernal_size % 2 != 1)
    {
        ROS_ERROR("Invalid kernal size: %i must be odd", config.blur_kernal_size);
        return;
    }

    if (config.resize_ratio >= 1.0 || config.resize_ratio <= 0.0)
        detector.needs_resize = false;
    else
    {
        detector.needs_resize = true;
        detector.resize_ratio = config.resize_ratio;
    }
    
    detector.updateResize();

    detector.median_blur_filter = cv::cuda::createMedianFilter(CV_8U, config.blur_kernal_size);
    detector.min_area         = config.min_area;
    detector.threshold_value  = config.threshold_value;
}


void paramToMatrix(ros::NodeHandle &nh, std::string &&param, float m[3][3]){
    std::string file;
    if (!nh.getParam(param, file)){
        ROS_FATAL("Could not get calibration file name (%s)", param.c_str());
    } else{
        std::ifstream fs(file);
        for (int i = 0; i < 3; i++)
        {
            fs >> m[i][0] >> m[i][1] >> m[i][2];
        }
    }
}


void paramToString(ros::NodeHandle &nh, std::string &&param, std::string &st) {
    std::string temp_str;
    if (!nh.getParam(param, temp_str)) {
        ROS_FATAL("Could not retrieve %s", param.c_str());
    } else {
        st = temp_str;
    }
}


int main(int argc, char** argv)
{
    // initialize init, TODO rename point_cloud_right
    ros::init(argc, argv, "point_cloud_right");

    // setup dynamic reconfigure
    dynamic_reconfigure::Server<uc_vision::LineDetectorConfig> dyn_server;
    dynamic_reconfigure::Server<uc_vision::LineDetectorConfig>::CallbackType f;

    f = boost::bind(&dynamicCallback, _1, _2);

    dyn_server.setCallback(f);

    ros::NodeHandle n("~");

    detector.pointcloudPublisher = n.advertise<sensor_msgs::PointCloud2>("point_cloud", 10);

    //get points to mask bot
    paramToMatrix(n, "calibration", detector.m);
    paramToString(n, "frame_id", detector.frame_id);

    detector.updateResize();

    detector.populateKernels();

    if (n.param("use_raw", false))
    {
        // Raw camera feed

        CameraFrameGroup g;

        Camera c;
        c.ld = &detector;
        n.getParam("dev", c.devName);
        n.getParam("width", c.width);
        n.getParam("height", c.height);
        n.getParam("fps", c.fps);

        if (c.initCamera())
        {
            ROS_FATAL("Camera failed to initialize: %s", strerror(errno));
            return -1;
        }

        g.add(c);

        if (g.start())
        {
            ROS_FATAL("Camera failed to start: %s", strerror(errno));
            return -1;
        }

        while (n.ok())
        {
            if (g.grabFrames<1>())
            {
                ROS_ERROR("Error grabbing frames: %s", strerror(errno));
                ros::Duration(0.1).sleep();
            }
        }
        if (g.stop())
        {
            ROS_FATAL("Camera failed to stop: %s", strerror(errno));
            return -1;
        }

        c.releaseCamera();
    }
    else
    {
        paramToString(n, "camera_feed", detector.camera_feed);

        // ROS based image subscription
        image_transport::ImageTransport it(n);
        image_transport::Subscriber sub = it.subscribe(detector.camera_feed, 1, subscriberCallback);

        ros::spin();
    }


    return 0;

}
