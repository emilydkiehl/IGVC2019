#include "camera.h"

#include <linux/videodev2.h>

#include "cap.h"

#include "opencv2/opencv.hpp"

Camera::Camera() : capHandle(NULL){}
CameraFrameGroup::CameraFrameGroup():handles() {}

int Camera::initCamera()
{
    if (this->capHandle != NULL)
        return 0;

    return init_camera(&this->capHandle, this->devName.c_str(), 
            this->width, this->height, V4L2_PIX_FMT_YUYV, this->fps);
}

int Camera::releaseCamera()
{
    if (this->capHandle == NULL)
        return 0;

    int r = free_camera(this->capHandle);
    this->capHandle = NULL;
    return r;
}

void CameraFrameGroup::add(Camera &c)
{
    struct handleref r;
    r.handle = c.capHandle;
    r.userdata = (void*)&c;

    this->handles.push_back(r);
}

int CameraFrameGroup::start()
{
    for (auto c = this->handles.begin(); c != this->handles.end(); c++)
    {
        if (start_capturing(c->handle))
	    return -1;
    }
}

int CameraFrameGroup::stop()
{
    for (auto c = this->handles.begin(); c != this->handles.end(); c++)
    {
        if (stop_capturing(c->handle))
	    return -1;
    }

}

void Camera::onGrabFrame(struct cam_frame *f)
{
    cv::Mat m(this->height, this->width, CV_8UC2, (char*)f->data);
    cv::Mat cvted;
    cv::cvtColor(m, cvted, cv::COLOR_YUV2BGR_YUY2);
    ld->processImage(cvted);
}
