#pragma once

#include <string>
#include <vector>

#include "cap.h"
#include "LineDetector.h"

class CameraFrameGroup;

class Camera
{

public:
    Camera();

    LineDetector *ld;
    std::string devName;
    int width;
    int height;
    int fps;

    int initCamera();
    int releaseCamera();

    // f is only valid from within this context, 
    // is requeued immediately following
    void onGrabFrame(struct cam_frame *f);

private:
    friend CameraFrameGroup;
    void *capHandle;
};

class CameraFrameGroup
{
    public:
        CameraFrameGroup();

        void add(Camera &c);
	int start();
	int stop();
        
        template<size_t i>
        int grabFrames()
        {
            struct cam_frame *frames[i];
            int size = i;
            int r = get_frame(this->handles.data(), this->handles.size(),
                    frames, &size);
            if (r)
                return r;

            for (int q = 0; q < size; q++)
            {
                ((Camera*)frames[q]->handle->userdata)->onGrabFrame(frames[q]);
                if (release_frame(frames[q]))
		{
                    ROS_ERROR("Error while releasing frame %s", strerror(errno));
		}
            }
            return 0;
        }

    private:
        std::vector<struct handleref> handles;
};
