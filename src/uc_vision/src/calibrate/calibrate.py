#!/usr/bin/env python
"""
Example: rosrun dokalman_vision calibrate.py -t /runtimeCameraDriverLeft/image_rect_color -d 8 6 -s 0.756 0.54 -a 50 -e 5.9
"""


import numpy as np
import cv2
import math
import pprint
import sys
import rospy
import yaml
import time

import argparse

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

pp = pprint.PrettyPrinter(indent=4)
font = cv2.FONT_HERSHEY_SIMPLEX

# src is a np.float32[n, 3]
# SRC IS OVERWRITTEN IN THIS FUNCTION
# m is a translation matrix
def translate_perspective_point(src, m):
    inter = np.empty([3, src.shape[0]], dtype=float)
    src = np.transpose(src)
    np.dot(m, src, inter)
    np.divide(inter[0, :], inter[2, :], src[0, :])
    np.divide(inter[1, :], inter[2, :], src[1, :])
    return np.transpose(src)

# image coordinates use 3 numbers - [x, y, 1]
def generate_perspective_matrix(src, board_dim, src_physical_center, src_image_center):
    # first, we generate an intermediate perspective. it has the following
    # properties:
    # the perspective plane is corrected
    # the size of the plane is corrected to meters
    # the origin is at the bottom left of the chessboard in relation to the
    # camera's image
    width = board_dim[0]
    height = board_dim[1]

    ref_dst = np.float32([
        [height,  width/2, 1], # top left
        [height, -width/2, 1], # top right
        [     0, -width/2, 1], # bottom right
        [     0,  width/2, 1], # bottom left
        ])

    int_m = cv2.getPerspectiveTransform(src, np.copy(ref_dst[:,:2]))

    # figure out where we are centered in within the intermediate frame
    int_center = np.copy(np.resize(src_image_center, [1, 3]))
    int_center_above = np.copy(int_center)
    int_center_above[0][0] += 100
    # pixel -> intermediate
    int_center = translate_perspective_point(int_center, int_m)
    int_center_above = translate_perspective_point(int_center_above, int_m)

    angle = -math.atan2(int_center_above[0][1] - int_center[0][1],
            int_center_above[0][0] - int_center[0][0])

    print("angle: {}".format(math.degrees(angle)))

    # first correct the angle of the board
    int_rotation = np.float32([
        [math.cos(angle), -math.sin(angle), 0],
        [math.sin(angle),  math.cos(angle), 0],
        [              0,                0, 1], # z is always 1!
        ])
    rot_dst = np.dot(int_rotation, np.transpose(ref_dst))
    rot_m = cv2.getPerspectiveTransform(src, np.copy(np.transpose(rot_dst)[:,:2]))

    rot_center = np.copy(np.resize(src_image_center, [1, 3]))
    rot_center = translate_perspective_point(rot_center, rot_m)

    xoff = src_physical_center[0] - int_center[0, 0]
    yoff = src_physical_center[1] - int_center[0, 1]

    print("icenter: {}".format(int_center))
    print("xoff: {} yoff: {}".format(xoff, yoff))

    rot_translation = np.float32([
        [1, 0, xoff],
        [0, 1, yoff],
        [0, 0,    1], # z is always 1!
        ])

    adj_dst = np.dot(rot_translation, rot_dst)
    dst = np.copy(np.transpose(adj_dst)[:,:2])

    m = cv2.getPerspectiveTransform(src, dst)
    r_center = np.copy(np.resize(src_image_center, [1, 3]))
    r_center = translate_perspective_point(r_center, m)
    print("rcenter: {}".format(r_center))
    return m

class calibration(object):
    def __init__(self, file_name, src_physical_center, board_dim, nx=8, ny=6):

        self.src_physical_center = src_physical_center
        self.board_dim = board_dim

        #Number of squares for chessboard
        self.nx = nx
        self.ny = ny

        self.file_name = file_name

        # Number of times to capture
        self.n = 30

        self.src = None
        self.dst = None

        self.M = None
        self.avg_src = np.zeros((4, 2), dtype=np.float32)

        self.counter_of_src = 0


    def perspectiveTransform(self, undistorted):
        gray = cv2.cvtColor(undistorted, cv2.COLOR_BGR2GRAY)

        ret, corners = cv2.findChessboardCorners(gray, (self.nx, self.ny), cv2.CALIB_CB_FAST_CHECK)
        warped = None

        cv2.putText(undistorted, str(self.counter_of_src)+"/{}".format(self.n),(60,60), font, 2,(0,0,255),2,cv2.LINE_AA)


        if ret == True:
            self.src_image_center = np.float32([gray.shape[0] / 2, gray.shape[1] / 2, 1])
            cv2.drawChessboardCorners(undistorted, (self.nx, self.ny), corners, ret)

            offset = 100

            img_size = (gray.shape[1], gray.shape[0])

            self.src = np.float32([corners[0][0], corners[self.nx-1][0], corners[-1][0], corners[-self.nx][0]])

            self.dst = np.float32([[offset, offset], [img_size[0] - offset,
                              offset], [img_size[0] - offset, img_size[1] - offset], [offset, img_size[1] - offset]])

            self.avg_src = np.add(self.avg_src, self.src)
            self.counter_of_src += 1

            self.M = cv2.getPerspectiveTransform(self.src, self.dst)
            warped = cv2.warpPerspective(undistorted, self.M, img_size)

            if self.counter_of_src >= self.n:
                self.average(undistorted, img_size)

        return warped

    def average(self, undistorted, img_size):
        self.avg_src /= self.counter_of_src

        self.M = generate_perspective_matrix(self.avg_src, self.board_dim,
                self.src_physical_center, self.src_image_center)

        self.save_data()

    def save_data(self):

        pp.pprint(self.M)
        pp.pprint(self.src)
        pp.pprint(self.dst)

        dataMap = {
            "m": self.M.tolist(),
            "src": self.src.tolist(),
            "dst": self.dst.tolist()
        }

        np.savez(self.file_name + "Transform",
                 transform=self.M,
                 src=self.src,
                 dst=self.dst)

        with open(self.file_name + "Transform.yaml", "w") as f:
            yaml.dump(dataMap, f)

        print("Saving the data as: {}Transform.npz".format(self.file_name))
        rospy.signal_shutdown("Saving the data as: {}Calibration.npz".format(self.file_name))


class ImageConverter(object):
    def __init__(self, args):

        self.bridge = CvBridge()

        self.name = args.topic.split('/')[1]

        self.calib = calibration(
                self.name,
                (math.tan(math.radians(args.angle)) * args.height, 0),
                args.boardsize,
                args.dimensions[0],
                args.dimensions[1],
                )

        self.vision_driver = rospy.Subscriber("{}".format(args.topic), Image, self.callback)

    def callback(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        warped = self.calib.perspectiveTransform(cv_image)
        if warped is not None:
            cv2.imshow("Warped", warped)
            pass

        cv2.imshow("Image window", cv_image)
        cv2.waitKey(1)

def cli():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t",
                        "--topic",
                        type=str,
                        help="rostopic for image feed e.g. --topic camera/image_raw",
                        )

    parser.add_argument("-d",
                        "--dimensions",
                        nargs="+",
                        type=int,
                        help="dimensions e.g. --dimensions 9 5",
                        required=True
                        )

    parser.add_argument("-s",
                        "--boardsize",
                        nargs="+",
                        type=float,
                        help="size of board e.g. --boardsize 9 5",
                        required=True
                        )

    parser.add_argument("-a",
                        "--angle",
                        type=float,
                        help="angle above vertical e.g. --angle 50",
                        required=True
                        )

    parser.add_argument("-e",
                        "--height",
                        type=float,
                        help="height above ground e.g. --height 3",
                        required=True
                        )

    args = parser.parse_args()

    def panic(s):
        rospy.roserror(s)
        rospy.signal_shutdown(s)

    # If  args are not of dimension size
    if (len(args.dimensions) != 2):
        panic("Dimensions can only be of size 2")
    else:
        args.dimensions = tuple(args.dimensions)

    if(args.topic is None):
        panic("Topic must be given")

    print(args.boardsize)
    if len(args.boardsize) != 2:
        panic("boardsize can only be size 2")
    else:
        args.boardsize = tuple(args.boardsize)

    if args.angle is None:
        panic("Angle must be given")

    if args.height is None:
        panic("Height must be given")

    return args


def main():
    rospy.init_node('dokalman_calibrate',anonymous=True)
    args = cli()
    ic = ImageConverter(args)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("ShuttingDown")

if __name__ == "__main__":
    main()
