# uc_msgs

This package contains all custom messages and services for the robot.

## svc/CoprocessorMsg.svc

This message is for sending messages over the coprocessor serial line. It is
compatible with the [kangaroo packet
format](https://www.dimensionengineering.com/datasheets/KangarooPacketSerialReference.pdf).
A few changes have been made to make the messages easier to use:

+ The CRC is handled by the driver. No CRC field is needed
+ The channel is a string tag which is configured and keyed against the driver.
  This is so the channels are configurable without changing the interfacing
  nodes.
+ While `command` is a int8, only values 0-127 are valid (as per the kangaroo
  spec).
+ The `reply` field specifies whether or not the driver needs to wait for a
  reply. If `reply` is true, then the response will be a single field `data`
  which contains the reply. If `reply` is false, the response will be zero
  length.
