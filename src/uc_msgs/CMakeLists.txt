cmake_minimum_required(VERSION 2.8.3)
project(uc_msgs)

find_package(catkin REQUIRED COMPONENTS
    std_msgs
    message_generation)

add_message_files(
  FILES
  InputStatus.msg
  CoprocessorStatus.msg
)

add_service_files(
  FILES
  CoprocessorMsg.srv
  S3Get.srv
  S3List.srv
  S3Set.srv
)

generate_messages(
  DEPENDENCIES
  std_msgs
)

catkin_package(
  CATKIN_DEPENDS std_msgs
)

install(DIRECTORY msg/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION})

install(DIRECTORY srv/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION})
