# Scripts

+ [dbuild](./dbuild) - Builds this repository's docker file and stores it as
  ucrobotics-ros. 
+ [dclean](./dclean) - Shuts down and removes all previous ucrobotics docker
  containers, as well as the ucrobotics-ros image.
+ [drun](./drun) - Runs the specified command (or bash) in either a previously
  running container or in a newly created ros container.

## Dxxxx Commands

The `dxxxx` commands (`dbuild`, `dclean`, and `drun`) are for interacting with a
ROS docker image. This is nice if you have an operating system that you do not
want to install ROS on but still want to compile and test changes. First, after
cloning this repository, you should run `dbuild`. This builds a local ROS image
on your computer. Then, whenever you want to open up the container, run `drun`
from anywhere. You can run this whenever and as many times as you want, and it
will give you a new terminal in the same container. If for whatever reason you
want to delete the image and container, you can run `dclean`, which will clean
both the container and the image from your computer. If you want to run `drun`
after this, you will have to run `dbuild` again to create a new image.

## Creating a new script

This repository has simple rules for creating a new script within it:

+ Write your script. It should use lowerCamelCase for naming, containing no
  spaces. The script should be marked as executable (`chmod +x <filename>`), and
  should have a [shebang](https://en.wikipedia.org/wiki/Shebang_(Unix))
  signifying what language the script is written in. File extensions are not
  required (however if you have one, please make it the correct one!)
+ Add a link to the project to this file under the subprojects list using the
  following format:
```
+ [project name](./directory name) - short description of the project
```
