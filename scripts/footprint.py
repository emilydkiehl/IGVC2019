#!/usr/bin/env python3

#               A
#             -----
#            /     \
#        B  /       \
#          /         \
#         /           \
#        /             \
#        |              |
#        |              |
#        | C    .       |
#        |      D       |
#        +--------------+
#               E

A = 0.2286
B = 0.508
C = 0.4572
D = 0.2032
E = 0.6604

RIGHT = E / 2
LEFT = -RIGHT

RIGHT_TOP = (A / 2)
LEFT_TOP = -(A / 2)
import math
DIST = RIGHT - RIGHT_TOP
TOP = C - D + math.sqrt((B * B) - (DIST * DIST))

MIDDLE = C - D

def print_point(x, y):
    print("- [{},{}]".format(x, y))

print_point(LEFT, -D)
print_point(LEFT, MIDDLE)
print_point(LEFT_TOP, TOP)
print_point(RIGHT_TOP, TOP)
print_point(RIGHT, MIDDLE)
print_point(RIGHT, -D)
